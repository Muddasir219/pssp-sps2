﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PSSPSAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace PSSPSAP.Controllers
{
    public class BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            
            if (!User.Identity.IsAuthenticated)
            {
                requestContext.HttpContext.Response.Redirect(Url.Action("Login", "Account", new { area = "" }));
            }
        }
        public Models.ApplicationUser GetUser()
        {
            return new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())).FindById(User.Identity.GetUserId());
        }
    }
}