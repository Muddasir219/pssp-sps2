﻿using PSSPSAP.Models;
using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class PaymentCalculationController : BaseController
    {
        PSSPSISEntities Db;
        public PaymentCalculationController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: PaymentCalculation
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult FindSchool(PaginationViewModel model)
        {

            try
            {
                List<string> dis = (model.Filters.Dist != null ? model.Filters.Dist.Select(x => x.PKCODE).ToList() : new List<string>());
                List<string> phases = (model.Filters.Phase != null ? model.Filters.Phase.Select(x => x.PKCODE).ToList() : new List<string>());
                List<decimal?> type = (model.Filters.appType != null ? model.Filters.appType.Select(x => x.PKCODE).ToList() : new List<decimal?>());

                var res = Db.V_TEMP_PAYMENT.AsQueryable();
                if (dis.Count > 0)
                    res = res.Where(x => dis.Contains(x.DISTRICT));
                if (phases.Count > 0)
                    res = res.Where(x => phases.Contains(x.PHASE));
                if (type.Count > 0)
                    res = res.Where(x => type.Contains(x.LTYPE));

                //var pcd = DateTime.Now.ToString("yyyy");
                //var mcd = DateTime.Now.Month.ToString();
                //res = res.Where(x => x.FKPCD == pcd);
                //res = res.Where(x => x.FKMCD == mcd);

                if (!string.IsNullOrEmpty(model.Search))
                {
                    res = res.Where(x => x.EMISCODE.ToLower().Contains(model.Search.ToLower())
                    || x.SCHOOL_NAME.ToLower().Contains(model.Search.ToLower())
                    || x.DISTRICT_NAME.ToLower().Contains(model.Search.ToLower())
                     || x.TEHSIL_NAME.ToLower().Contains(model.Search.ToLower())
                      || x.LTYPE_NAME.ToLower().Contains(model.Search.ToLower())


                    );
                }
                var totalCont = res.Count();
                //var query = Db.V_TEMP_PAYMENT.AsQueryable().ToString();
                //var res = Db.V_TEMP_PAYMENT.ToList();
             
                var fList = res.OrderBy(x=> x.EMISCODE).Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();
                // var RES = res.ToList();
                return Json(new { res = fList, TotalCount = totalCont, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", TotalCount = 0, Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
           
        }
        [HttpPost]
        public JsonResult Calculate(string emis, int? StuQty)
        {
            try
            {
                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();
                var school = Db.V_TEMP_PAYMENT.FirstOrDefault(x => x.EMISCODE == emis);
                int mc = 0;
                //Basic Rate Based on School Lavel
                int basicRate = 0;
                int basicRateS = 0;
                var rates = Db.PAYMENT_RATES.Where(x => x.APPLICANT_TYPE == school.LTYPE && x.FKPCD == pcd && x.FKMCD == mcd).ToList();
                if (rates.Count == 0)
                      return Json(new { pay = "", Pdtl = "", Pnalty = "", BR = "", MC = "", Err = "No Rates Define for this type" }, JsonRequestBehavior.AllowGet);

                    basicRate = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Primary")).RATE;
                basicRateS = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Secondary")).RATE;
                mc = rates.FirstOrDefault(x => x.FKTYPE == "10101") != null ? (int)rates.FirstOrDefault(x => x.FKTYPE == "10101").RATE : 0;

                var payment =  Calculation(school, StuQty);
                var DTL = Db.PAYMENTS_DTL.
                        Where(x => x.FK_SRNO == payment.SRNO && x.FKPLANTY == null).
                        Select(o => new { ID = o.SRNO, Name = o.ADJUSTMENT.NAME, Amount = o.AMNT, IAMNT = o.IAMNT }).ToList();
                var PDTL = Db.PAYMENTS_DTL.
                        Where(x => x.FK_SRNO == payment.SRNO && x.FK_ADJUST == null).
                        Select(o => new { ID = o.SRNO, Name = o.PLANTy.INDICATOR, Amount = o.PLANTY_AMT, IAMNT = o.PLANTY_PER }).ToList();
                var P = new
                {
                    SRNO = payment.SRNO,
                    PRIMARY_ENROL = payment.PRIMARY_ENROL,
                    PRIMARY_ENROL_PAID = payment.PRIMARY_ENROL_PAID,
                    PRIMARY_PAYMENT = payment.PRIMARY_PAYMENT,
                    SECONDARY_ENROL = payment.SECONDARY_ENROL,
                    SECONDARY_ENROL_PAID = payment.SECONDARY_ENROL_PAID,
                    SECONDARY_PAYMENT = payment.SECONDARY_PAYMENT,
                    MANAGEMENT_FEE = payment.MANAGEMENT_FEE,
                    GROSS = payment.GROSS,
                    DEDUCTIONS = payment.DEDUCTIONS,
                    PLANTY_DEDUCTION = payment.PLANTY_DEDUCTION,
                    RMKS = payment.RMKS,
                    PLANTY_RMKS = payment.PLANTY_RMKS,
                    NET_PAYMENT = payment.NET_PAYMENT,
                    RELEASE_PAYMENT = payment.RELEASE_PAYMENT,
                    STOP_PAYMENT = payment.STOP_PAYMENT,
                    WITHHOLD = payment.WITHHOLD,
                    PAYMENTBASEDON = payment.PAYMENTBASEDON
                    
                };

                return Json(new { pay = P, Pdtl = DTL, Pnalty = PDTL, BR = basicRate, BRS = basicRateS, MC = mc, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { pay = "", Pdtl = "", Pnalty = "", BR = "", BRS = "" , MC = "", Err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult CalculateAllPayments(SearchViewModel model)
        {
            try
            {

                List<string> dis = (model.Dist != null ? model.Dist.Select(x => x.PKCODE).ToList() : new List<string>());
                List<string> phases = (model.Phase != null ? model.Phase.Select(x => x.PKCODE).ToList() : new List<string>());
                List<decimal?> type = (model.appType != null ? model.appType.Select(x => x.PKCODE).ToList() : new List<decimal?>());

                var res = Db.V_TEMP_PAYMENT.AsQueryable();
                if (dis.Count > 0)
                    res = res.Where(x => dis.Contains(x.DISTRICT));
                if (phases.Count > 0)
                    res = res.Where(x => phases.Contains(x.PHASE));
                if (type.Count > 0)
                    res = res.Where(x => type.Contains(x.LTYPE));


                var pcd = DateTime.Now.ToString("yy");
                var pcd1 = DateTime.Now.ToString("yyyy");
                var mcd = DateTime.Now.Month.ToString();
                res = res.Where(x => x.FKPCD == pcd1);
                res = res.Where(x => x.FKMCD == mcd);
                var schools =res.ToList();
                int mc = 0;
                foreach(var s in schools) {
                    //Basic Rate Based on School Lavel
                    int basicRate = 0;
                    int basicRateS = 0;
                    var rates = Db.PAYMENT_RATES.Where(x => x.APPLICANT_TYPE == s.LTYPE && x.FKPCD == pcd && x.FKMCD == mcd).ToList();
                    if (rates.Count == 0)
                        continue;
                    basicRate = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Primary")).RATE;
                    basicRateS = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Secondary")).RATE;
                    mc = rates.FirstOrDefault(x => x.FKTYPE == "10101") != null ? (int)rates.FirstOrDefault(x => x.FKTYPE == "10101").RATE : 0;
                    // var payment = Db.PAYMENTS.FirstOrDefault(x => x.FKPCD == pcd && x.FKMCD == mcd && x.EMISCODE == s.EMISCODE);
                    var p = Calculation(s, null);
                    //List<PAYMENTS_DTL> dudDtl = new List<PAYMENTS_DTL>();
                    //if (payment == null)
                    //{
                    //    payment = new PAYMENT();
                    //    //Student Count To be Paid
                    //    int stu = 99999;
                      
                    //    if (s.PRIMARY_STD < stu && s.PRIMARY_STD != 0)
                    //        stu = (int)s.PRIMARY_STD;
                    //    if (s.PSSPPMIU_STDNO < stu && s.PSSPPMIU_STDNO != 0)
                    //        stu = (int)s.PSSPPMIU_STDNO;
                    //    if (s.MNE_STU < stu && s.MNE_STU != 0)
                    //        stu = (int)s.MNE_STU;
                    //    //Deduction Calculation for Adjustment
                    //    // List<PAYMENTS_DTL> dudDtl = new List<PAYMENTS_DTL>();
                    //    int deduction = 0;
                    //    var dudList = Db.ADJ_EMIS_DTL.Where(x => x.EMISCODE == s.EMISCODE).ToList();
                    //    foreach (var dud in dudList)
                    //    {
                    //        var pdtl = new PAYMENTS_DTL();
                    //        pdtl.FKMCD = mcd;
                    //        pdtl.FKPCD = pcd;
                    //        pdtl.FK_ADJUST = dud.ADJUSTMENT.ID;
                    //        pdtl.AMNT = dud.ADJUSTMENT.AMNT;
                    //        pdtl.IAMNT = dud.ADJUSTMENT.IAMNT;
                    //        pdtl.ENABLE_FLAG = "Y";
                    //        pdtl.CREATED_BY = GetUser().Id;
                    //        dudDtl.Add(pdtl);
                    //        deduction += (int)dud.ADJUSTMENT.IAMNT;
                    //    }
                    //    var overAll = Db.ADJ_EMIS_DTL.Where(x => x.RMKS == "All").ToList();
                    //    foreach (var dud in overAll)
                    //    {
                    //        var pdtl = new PAYMENTS_DTL();
                    //        pdtl.FKMCD = mcd;
                    //        pdtl.FKPCD = pcd;
                    //        pdtl.FK_ADJUST = dud.ADJUSTMENT.ID;
                    //        pdtl.AMNT = dud.ADJUSTMENT.AMNT;
                    //        pdtl.IAMNT = dud.ADJUSTMENT.IAMNT;
                    //        pdtl.ENABLE_FLAG = "Y";
                    //        pdtl.CREATED_BY = GetUser().Id;
                    //        dudDtl.Add(pdtl);
                    //        deduction += (int)dud.ADJUSTMENT.IAMNT;

                    //    }
                    //    payment.FKMCD = mcd;
                    //    payment.FKPCD = pcd;
                    //    payment.EMISCODE = s.EMISCODE;
                    //    //Primary
                    //    payment.PRIMARY_ENROL = stu;
                    //    payment.PRIMARY_ENROL_PAID = stu;
                    //    basicRate = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Primary")).RATE;
                    //    payment.PRIMARY_PAYMENT = basicRate * stu;
                    //    //Secondry
                    //    payment.SECONDARY_ENROL = s.SECONDRY_STD;
                    //    payment.SECONDARY_ENROL_PAID = s.SECONDRY_STD;
                    //    payment.MANAGEMENT_FEE = 0;
                    //    basicRateS = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Secondary")).RATE;
                    //    payment.SECONDARY_PAYMENT = basicRateS * s.SECONDRY_STD;



                    //    int cindex = 1;
                    //    decimal? OtherP = 0;
                    //    foreach (var r in rates)
                    //    {
                    //        if (!r.SPS_TYPE.NAME.Contains("Basic Cost Primary") && !r.SPS_TYPE.NAME.Contains("Basic Cost Secondary") && r.FKTYPE != "10101")
                    //        {
                    //            if (r.UNIT == "%")
                    //            {
                    //                OtherP += (payment.PRIMARY_PAYMENT * r.RATE / 100);
                    //                payment.RMKS += cindex + " => " + r.SPS_TYPE.NAME + "@" + r.RATE + "/" + r.UNIT + " on Primary Payment.</br>";
                    //            }
                    //            else
                    //            {
                    //                OtherP += payment.PRIMARY_ENROL * r.RATE;
                    //                payment.RMKS += cindex + " => " + r.SPS_TYPE.NAME + "@" + r.RATE + "/" + r.UNIT + " on Primary Enrollment .</br>";
                    //            }
                    //            cindex++;

                    //        }
                    //        if (r.FKTYPE == "10101")
                    //        {
                    //            if (s.IS_CHAIN == 1)
                    //            {
                    //                payment.MANAGEMENT_FEE = (payment.PRIMARY_PAYMENT * r.RATE / 100);
                    //                mc = (int)r.RATE;
                    //            }
                    //            else
                    //            {
                    //                payment.MANAGEMENT_FEE = 0;
                    //            }
                    //        }

                    //    }

                    //    payment.OTHER_PAYMENT = OtherP;
                    //    payment.GROSS = payment.PRIMARY_PAYMENT + payment.MANAGEMENT_FEE + payment.OTHER_PAYMENT + payment.SECONDARY_PAYMENT;
                    //    payment.DEDUCTIONS = deduction;
                    //    payment.NET_PAYMENT = payment.GROSS - payment.DEDUCTIONS;
                    //    //Pnalty Calculation
                    //    int pindex = 1;
                    //    int PAMNT = 0;
                    //    var PlantyList = Db.PLANTIES_DTL.Where(x => x.EMISCODE == s.EMISCODE && x.FKMCD == mcd && x.FKPCD == pcd).ToList();
                    //    foreach (var dud in PlantyList)
                    //    {
                    //        var pdtl = new PAYMENTS_DTL();
                    //        pdtl.FKMCD = mcd;
                    //        pdtl.FKPCD = pcd;
                    //        pdtl.FKPLANTY = dud.PLANTy.ID;
                    //        pdtl.PLANTY_PER = dud.PLANTy.PENALTY;
                    //        pdtl.PLANTY_AMT = payment.GROSS * dud.PLANTy.PENALTY / 100;
                    //        PAMNT += (int)pdtl.PLANTY_AMT;
                    //        payment.PLANTY_RMKS += pindex + "=>" + dud.PLANTy.INDICATOR + "@ of " + dud.PLANTy.PENALTY + "% on Net Payment</br>";
                    //        pdtl.ENABLE_FLAG = "Y";
                    //        pdtl.CREATED_BY = GetUser().Id;
                    //        dudDtl.Add(pdtl);
                    //        pindex++;
                    //    }
                    //    var overAllP = Db.PLANTIES_DTL.Where(x => x.RMKS == "All" && x.FKMCD == mcd && x.FKPCD == pcd).ToList();
                    //    foreach (var dud in overAllP)
                    //    {
                    //        var pdtl = new PAYMENTS_DTL();
                    //        pdtl.FKMCD = mcd;
                    //        pdtl.FKPCD = pcd;
                    //        pdtl.FKPLANTY = dud.PLANTy.ID;
                    //        pdtl.PLANTY_PER = dud.PLANTy.PENALTY;
                    //        pdtl.PLANTY_AMT = payment.GROSS * dud.PLANTy.PENALTY / 100;
                    //        PAMNT += (int)pdtl.PLANTY_AMT;
                    //        payment.PLANTY_RMKS += pindex + "=>" + dud.PLANTy.INDICATOR + "@ of " + dud.PLANTy.PENALTY + "% on Net Payment</br>";
                    //        pdtl.ENABLE_FLAG = "Y";
                    //        pdtl.CREATED_BY = GetUser().Id;
                    //        dudDtl.Add(pdtl);
                    //        pindex++;
                    //    }
                    //    payment.PLANTY_DEDUCTION = PAMNT;
                    //    payment.NET_PAYMENT = payment.NET_PAYMENT - PAMNT;
                    //    if (payment.NET_PAYMENT < 0)
                    //    {
                    //        payment.WITHHOLD = payment.NET_PAYMENT * -1;
                    //        payment.NET_PAYMENT = 0;
                    //    }

                    //    payment.RELEASE_PAYMENT = payment.NET_PAYMENT;
                    
                    //    payment.STOP_PAYMENT = 0;
                    //    Db.PAYMENTS.Add(payment);
                    //    Db.SaveChanges();
                    //    foreach (var dtl in dudDtl)
                    //    {
                    //        dtl.FK_SRNO = payment.SRNO;
                    //        Db.PAYMENTS_DTL.Add(dtl);
                    //        Db.SaveChanges();
                    //    }
                    //}
                   


                }
               

                return Json(new { res = "Payments Calculate Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {res = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult StopRelease(int? SR , int? SRNO)
        {
            try {

                var payment = Db.PAYMENTS.FirstOrDefault(x => x.SRNO == SRNO);
                if (SR == 0)
                {
                    payment.RELEASE_PAYMENT = 0;
                    payment.STOP_PAYMENT = 1;
                }
                else
                {
                    payment.RELEASE_PAYMENT = 1;
                    payment.STOP_PAYMENT = 0;
                }
                
                return Json(new { res = "Status Change Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { res = "Status Change Successfully", Err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
         



        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }


        ///////////////////
        public PAYMENT Calculation(V_TEMP_PAYMENT school, int? StuQty)
        {
            var pcd = DateTime.Now.ToString("yy");
            var mcd = DateTime.Now.Month.ToString();
          //  var school = Db.V_TEMP_PAYMENT.FirstOrDefault(x => x.EMISCODE == emis);
            int mc = 0;

            //Basic Rate Based on School Lavel
            int basicRate = 0;
            int basicRateS = 0;
            var rates = Db.PAYMENT_RATES.Where(x => x.APPLICANT_TYPE == school.LTYPE && x.FKPCD == pcd && x.FKMCD == mcd).ToList();
            if (rates.Count == 0)
              //  return Json(new { pay = "", Pdtl = "", Pnalty = "", BR = "", MC = "", Err = "No Rates Define for this type" }, JsonRequestBehavior.AllowGet);

            basicRate = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Primary")).RATE;
            basicRateS = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Secondary")).RATE;
            mc = rates.FirstOrDefault(x => x.FKTYPE == "10101") != null ? (int)rates.FirstOrDefault(x => x.FKTYPE == "10101").RATE : 0;
            var payment = Db.PAYMENTS.FirstOrDefault(x => x.FKPCD == pcd && x.FKMCD == mcd && x.EMISCODE == school.EMISCODE);
            List<PAYMENTS_DTL> dudDtl = new List<PAYMENTS_DTL>();
            if (payment == null)
            {
                payment = new PAYMENT();
                //Student Count To be Paid

                int Absentism = (int)school.ABSENTISM_PER_AGE;

                int stu = 99999;
                string basedOn = "";
                if (school.PRIMARY_STD < stu && school.PRIMARY_STD != 0)
                {
                    stu = (int)school.PRIMARY_STD;
                    basedOn = "Monthly Report";
                    if (Absentism < 70)
                    {
                        stu = (int)school.STNO_PRASENT;
                        basedOn = "Attendance of the day PMIU";
                    }

                }
                if (school.PSSPPMIU_STDNO < stu && school.PSSPPMIU_STDNO != 0)
                {
                    stu = (int)school.PSSPPMIU_STDNO;
                    basedOn = "PMIU";
                    if (Absentism < 70)
                    {
                        stu = (int)school.STNO_PRASENT;
                        basedOn = "Attendance of the day PMIU";
                    }
                }
                if (school.MNE_STU < stu && school.MNE_STU != 0)
                {
                    stu = (int)school.MNE_STU;
                    basedOn = "M & E";
                    if (school.MNE_STU_ABSENTISM_PERSANTAGE < 70)
                    {
                        stu = (int)school.MNE_PRASENT_STU;
                        basedOn = "Attendance of the day M & E";
                    }
                }
                //Deduction Calculation for Adjustment
                // List<PAYMENTS_DTL> dudDtl = new List<PAYMENTS_DTL>();
                int deduction = 0;
                var dudList = Db.ADJ_EMIS_DTL.Where(x => x.EMISCODE == school.EMISCODE).ToList();
                foreach (var dud in dudList)
                {
                    var pdtl = new PAYMENTS_DTL();
                    pdtl.FKMCD = mcd;
                    pdtl.FKPCD = pcd;
                    pdtl.FK_ADJUST = dud.ADJUSTMENT.ID;
                    pdtl.AMNT = dud.ADJUSTMENT.AMNT;
                    pdtl.IAMNT = dud.ADJUSTMENT.IAMNT;
                    pdtl.ENABLE_FLAG = "Y";
                    pdtl.CREATED_BY = GetUser().Id;
                    dudDtl.Add(pdtl);
                    deduction += (int)dud.ADJUSTMENT.IAMNT;
                }
                var overAll = Db.ADJ_EMIS_DTL.Where(x => x.RMKS == "All").ToList();
                foreach (var dud in overAll)
                {
                    var pdtl = new PAYMENTS_DTL();
                    pdtl.FKMCD = mcd;
                    pdtl.FKPCD = pcd;
                    pdtl.FK_ADJUST = dud.ADJUSTMENT.ID;
                    pdtl.AMNT = dud.ADJUSTMENT.AMNT;
                    pdtl.IAMNT = dud.ADJUSTMENT.IAMNT;
                    pdtl.ENABLE_FLAG = "Y";
                    pdtl.CREATED_BY = GetUser().Id;
                    dudDtl.Add(pdtl);
                    deduction += (int)dud.ADJUSTMENT.IAMNT;

                }
                payment.FKMCD = mcd;
                payment.FKPCD = pcd;
                payment.PAYMENTBASEDON = basedOn;
                payment.EMISCODE = school.EMISCODE;
                //Primary
                payment.PRIMARY_ENROL = stu;
                payment.PRIMARY_ENROL_PAID = stu;
                basicRate = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Primary")).RATE;
                payment.PRIMARY_PAYMENT = basicRate * stu;
                //Secondry
                payment.SECONDARY_ENROL = school.SECONDRY_STD;
                payment.SECONDARY_ENROL_PAID = school.SECONDRY_STD;
                payment.MANAGEMENT_FEE = 0;
                basicRateS = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Secondary")).RATE;
                payment.SECONDARY_PAYMENT = basicRateS * school.SECONDRY_STD;



                int cindex = 1;
                decimal? OtherP = 0;
                foreach (var r in rates)
                {
                    if (!r.SPS_TYPE.NAME.Contains("Basic Cost Primary") && !r.SPS_TYPE.NAME.Contains("Basic Cost Secondary") && r.FKTYPE != "10101")
                    {
                        if (r.UNIT == "%")
                        {
                            OtherP += (payment.PRIMARY_PAYMENT * r.RATE / 100);
                            payment.RMKS += cindex + " => " + r.SPS_TYPE.NAME + "@" + r.RATE + "/" + r.UNIT + " on Primary Payment.</br>";
                        }
                        else
                        {
                            OtherP += payment.PRIMARY_ENROL * r.RATE;
                            payment.RMKS += cindex + " => " + r.SPS_TYPE.NAME + "@" + r.RATE + "/" + r.UNIT + " on Primary Enrollment .</br>";
                        }
                        cindex++;

                    }
                    if (r.FKTYPE == "10101")
                    {
                        if (school.IS_CHAIN == 1)
                        {
                            payment.MANAGEMENT_FEE = (payment.PRIMARY_PAYMENT * r.RATE / 100);
                            mc = (int)r.RATE;
                        }
                        else
                        {
                            payment.MANAGEMENT_FEE = 0;
                        }
                    }

                }

                payment.OTHER_PAYMENT = OtherP;
                payment.GROSS = payment.PRIMARY_PAYMENT + payment.MANAGEMENT_FEE + payment.OTHER_PAYMENT + payment.SECONDARY_PAYMENT;
                payment.DEDUCTIONS = deduction;
                payment.NET_PAYMENT = payment.GROSS - payment.DEDUCTIONS;
                //Pnalty Calculation
                int pindex = 1;
                int PAMNT = 0;
                var PlantyList = Db.PLANTIES_DTL.Where(x => x.EMISCODE == school.EMISCODE && x.FKMCD == mcd && x.FKPCD == pcd).ToList();
                foreach (var dud in PlantyList)
                {
                    var pdtl = new PAYMENTS_DTL();
                    pdtl.FKMCD = mcd;
                    pdtl.FKPCD = pcd;
                    pdtl.FKPLANTY = dud.PLANTy.ID;
                    pdtl.PLANTY_PER = dud.PLANTy.PENALTY;
                    pdtl.PLANTY_AMT = payment.GROSS * dud.PLANTy.PENALTY / 100;
                    PAMNT += (int)pdtl.PLANTY_AMT;
                    payment.PLANTY_RMKS += pindex + "=>" + dud.PLANTy.INDICATOR + "@ of " + dud.PLANTy.PENALTY + "% on Net Payment</br>";
                    pdtl.ENABLE_FLAG = "Y";
                    pdtl.CREATED_BY = GetUser().Id;
                    dudDtl.Add(pdtl);
                    pindex++;
                }
                var overAllP = Db.PLANTIES_DTL.Where(x => x.RMKS == "All" && x.FKMCD == mcd && x.FKPCD == pcd).ToList();
                foreach (var dud in overAllP)
                {
                    var pdtl = new PAYMENTS_DTL();
                    pdtl.FKMCD = mcd;
                    pdtl.FKPCD = pcd;
                    pdtl.FKPLANTY = dud.PLANTy.ID;
                    pdtl.PLANTY_PER = dud.PLANTy.PENALTY;
                    pdtl.PLANTY_AMT = payment.GROSS * dud.PLANTy.PENALTY / 100;
                    PAMNT += (int)pdtl.PLANTY_AMT;
                    payment.PLANTY_RMKS += pindex + "=>" + dud.PLANTy.INDICATOR + "@ of " + dud.PLANTy.PENALTY + "% on Net Payment</br>";
                    pdtl.ENABLE_FLAG = "Y";
                    pdtl.CREATED_BY = GetUser().Id;
                    dudDtl.Add(pdtl);
                    pindex++;
                }
                payment.PLANTY_DEDUCTION = PAMNT;
                payment.NET_PAYMENT = payment.NET_PAYMENT - PAMNT;
                if (payment.NET_PAYMENT < 0)
                {
                    payment.WITHHOLD = payment.NET_PAYMENT * -1;
                    payment.NET_PAYMENT = 0;
                }

                payment.RELEASE_PAYMENT = payment.NET_PAYMENT;
                payment.STOP_PAYMENT = 0;
                Db.PAYMENTS.Add(payment);
                Db.SaveChanges();
                foreach (var dtl in dudDtl)
                {
                    dtl.FK_SRNO = payment.SRNO;
                    Db.PAYMENTS_DTL.Add(dtl);
                    Db.SaveChanges();
                }
            }
            else // Edit Payment 
            {
                if (StuQty != null)
                {
                    payment.PRIMARY_ENROL = StuQty;
                    payment.PAYMENTBASEDON = "Manual";
                    payment.PRIMARY_ENROL_PAID = StuQty;
                    basicRate = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Primary")).RATE;
                    payment.PRIMARY_PAYMENT = basicRate * StuQty;

                    payment.SECONDARY_ENROL = school.SECONDRY_STD;
                    payment.SECONDARY_ENROL_PAID = school.SECONDRY_STD;
                    basicRateS = (int)rates.FirstOrDefault(x => x.SPS_TYPE.NAME.Contains("Basic Cost Secondary")).RATE;
                    payment.SECONDARY_PAYMENT = basicRateS * school.SECONDRY_STD;
                    payment.RMKS = "";
                    int cindex = 1;
                    decimal? OtherP = 0;
                    payment.MANAGEMENT_FEE = 0;
                    foreach (var r in rates)
                    {
                        if (!r.SPS_TYPE.NAME.Contains("Basic Cost Primary") && !r.SPS_TYPE.NAME.Contains("Basic Cost Secondary") && r.FKTYPE != "10101")
                        {
                            if (r.UNIT == "%")
                            {
                                OtherP += (payment.PRIMARY_PAYMENT * r.RATE / 100);
                                payment.RMKS += cindex + " => " + r.SPS_TYPE.NAME + "@" + r.RATE + "/" + r.UNIT + " on Primary Payment.</br>";
                            }
                            else
                            {
                                OtherP += payment.PRIMARY_ENROL * r.RATE;
                                payment.RMKS += cindex + " => " + r.SPS_TYPE.NAME + "@" + r.RATE + "/" + r.UNIT + " on Primary Enrollment .</br>";
                            }
                            cindex++;
                        }
                        if (r.FKTYPE == "10101")
                        {
                            if (school.IS_CHAIN == 1)
                            {
                                payment.MANAGEMENT_FEE = (payment.PRIMARY_PAYMENT * r.RATE / 100);
                                mc = (int)r.RATE;
                            }
                            else
                            {
                                payment.MANAGEMENT_FEE = 0;
                            }
                        }

                    }
                    payment.OTHER_PAYMENT = OtherP;
                    payment.GROSS = payment.PRIMARY_PAYMENT + payment.MANAGEMENT_FEE + payment.OTHER_PAYMENT + payment.SECONDARY_PAYMENT;
                    //Pnalty Calculation
                    payment.PLANTY_RMKS = "";
                    int pindex = 1;
                    int PAMNT = 0;
                    var PlantyList = Db.PLANTIES_DTL.Where(x => x.EMISCODE == school.EMISCODE && x.FKMCD == mcd && x.FKPCD == pcd).ToList();
                    foreach (var dud in PlantyList)
                    {
                        var pdtl = new PAYMENTS_DTL();
                        pdtl.FKMCD = mcd;
                        pdtl.FKPCD = pcd;
                        pdtl.FKPLANTY = dud.PLANTy.ID;
                        pdtl.PLANTY_PER = dud.PLANTy.PENALTY;
                        pdtl.PLANTY_AMT = payment.GROSS * dud.PLANTy.PENALTY / 100;
                        PAMNT += (int)pdtl.PLANTY_AMT;
                        payment.PLANTY_RMKS += pindex + "=>" + dud.PLANTy.INDICATOR + "@ of " + dud.PLANTy.PENALTY + "% on Net Payment</br>";
                        pdtl.ENABLE_FLAG = "Y";
                        pdtl.CREATED_BY = GetUser().Id;
                        dudDtl.Add(pdtl);
                        pindex++;
                    }
                    var overAllP = Db.PLANTIES_DTL.Where(x => x.RMKS == "All" && x.FKMCD == mcd && x.FKPCD == pcd).ToList();
                    foreach (var dud in overAllP)
                    {
                        var pdtl = new PAYMENTS_DTL();
                        pdtl.FKMCD = mcd;
                        pdtl.FKPCD = pcd;
                        pdtl.FKPLANTY = dud.PLANTy.ID;
                        pdtl.PLANTY_PER = dud.PLANTy.PENALTY;
                        pdtl.PLANTY_AMT = payment.GROSS * dud.PLANTy.PENALTY / 100;
                        PAMNT += (int)pdtl.PLANTY_AMT;
                        payment.PLANTY_RMKS += pindex + "=>" + dud.PLANTy.INDICATOR + "@ of " + dud.PLANTy.PENALTY + "% on Net Payment</br>";
                        pdtl.ENABLE_FLAG = "Y";
                        pdtl.CREATED_BY = GetUser().Id;
                        dudDtl.Add(pdtl);
                        pindex++;
                    }
                    payment.PLANTY_DEDUCTION = PAMNT;
                    payment.NET_PAYMENT = payment.GROSS - payment.DEDUCTIONS - payment.PLANTY_DEDUCTION;
                    if (payment.NET_PAYMENT < 0)
                    {
                        payment.WITHHOLD = payment.NET_PAYMENT * -1;
                        payment.NET_PAYMENT = 0;
                    }

                    payment.RELEASE_PAYMENT = payment.NET_PAYMENT;
                    Db.SaveChanges();
                    var OldP = Db.PAYMENTS_DTL.Where(x => x.FK_SRNO == payment.SRNO && x.FK_ADJUST == null).ToList();
                    Db.PAYMENTS_DTL.RemoveRange(OldP);
                    Db.SaveChanges();
                    foreach (var dtl in dudDtl)
                    {
                        dtl.FK_SRNO = payment.SRNO;
                        Db.PAYMENTS_DTL.Add(dtl);
                        Db.SaveChanges();
                    }
                }
            }
            return payment;
        }


    }

    public class FindSchoolViewModel
    {
        public string Dist { get; set; }
        public string Phase { get; set; }
        public int appType { get; set; }
              
    }
}