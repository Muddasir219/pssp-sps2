﻿using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class StopReleaseController : Controller
    {
        // GET: StopRelease
        PSSPSISEntities Db = new PSSPSISEntities();

        public ActionResult Index()
        {
            return View();
        }


        public JsonResult GetEmis(string id)
        {

            var x = Db.EMISSes.FirstOrDefault(o => o.PKCODE == id);
            if(x==null)
                return Json(new { res = "" , Err = "Invalid EMIS Code" }, JsonRequestBehavior.AllowGet);
            var sch =  new { ID = x.PKCODE, Name = x.NAME, Phase = x.SCHOOL_PHASE, Dist = x.GEOLVL.NAME , teh = x.GEOLVL1.NAME };               
            return Json(new { res = sch , Err = "N" }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetPayments(string id)
        {
            try
            {
                var result = Db.PAYMENTS.Where(x => x.EMISCODE == id)
                    .Select(o => new { SRNO = o.SRNO ,
                        EMISCODE = o.EMISCODE,
                        FKPCD = o.FKPCD,
                        FKMCD = o.FKMCD,
                        Payment = o.NET_PAYMENT,
                        RELEASE_PAYMENT = o.RELEASE_PAYMENT ,
                        RMKS = o.STOP_RELEASE_RMKS,
                        ADDITIONAL_PENALTY= o.ADDITIONAL_PENALTY,
                        ADDTIONAL_PENALTY_AMNT=o.ADDTIONAL_PENALTY_AMNT,
                        ADDTIONAL_PENALTY_RMKS=o.ADDTIONAL_PENALTY_RMKS
                    })
                    .ToList();
                return Json(new { res = result, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { res = "", Err = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveStopRealese(SRModel model)
        {
            try
            {
                var payment = Db.PAYMENTS.FirstOrDefault(x => x.SRNO == model.SRNO);
                if(payment==null)
                return Json(new { res = "Oops! Something went wrong try again.", Err = "N" }, JsonRequestBehavior.AllowGet);
                payment.RELEASE_PAYMENT = model.SR;
                payment.STOP_RELEASE_RMKS = model.RMKS;
                payment.ADDITIONAL_PENALTY = model.ADD_Pnalty;
                payment.ADDTIONAL_PENALTY_AMNT = model.ADD_Pnalty_AMT;
                payment.ADDTIONAL_PENALTY_RMKS = model.ADD_Pnalty_RMKS;
                Db.SaveChanges();
                return Json(new { res = "Payment Status Updated Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.Message+"-Inner Ex-"+ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
    


    public class SRModel
    {
        public int SRNO { get; set; }
        public int SR { get; set; }
        public string RMKS { get; set; }
        public decimal ADD_Pnalty { get; set; }
        public decimal ADD_Pnalty_AMT { get; set; }
        public string ADD_Pnalty_RMKS { get; set; }
    }
}