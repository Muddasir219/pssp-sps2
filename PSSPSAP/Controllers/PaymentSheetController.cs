﻿using PSSPSAP.Models;
using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class PaymentSheetController : Controller
    {
        public PSSPSISEntities Db;
        public PaymentSheetController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: PaymentSheet
        [HttpPost]
        public ActionResult Index(SearchViewModel model)
        {

            List<string> dis = (model.Dist != null ? model.Dist.Select(x => x.PKCODE).ToList() : new List<string>());
            List<string> phases = (model.Phase != null ? model.Phase.Select(x => x.PKCODE).ToList() : new List<string>());
            List<decimal?> type = (model.appType != null ? model.appType.Select(x => x.PKCODE).ToList() : new List<decimal?>());

            var res = Db.PAYMENTS.AsQueryable();
            if (dis.Count > 0)
                res = res.Where(x => dis.Contains(x.EMISS.GEOLVL.PKCODE));
            if (phases.Count > 0)
                res = res.Where(x => phases.Contains(x.EMISS.SCHOOL_PHASE));
            if (type.Count > 0)
                res = res.Where(x => type.Contains(x.EMISS.LICENSEE.LTYPE));

            ViewBag.Filter = model;
            var RES = res.ToList();
            return View(RES);
        }
    }
}