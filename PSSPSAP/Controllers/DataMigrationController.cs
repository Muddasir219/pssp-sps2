﻿using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class DataMigrationController : Controller
    {
        public PSSPSISEntities Db;
        public DataMigrationController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: DataMigration
        public string Index()
        {
            try
            {
                string notfound = "";
                int processed = 0;
               // var bridg = Db.Database.SqlQuery<ALL_SCHOOL_TEMP_DATA2>("SELECT DISTINCT(X.LINCENSEE_ID)  LINCENSEE_ID,(SELECT SCHOOL_CODE FROM PSSPSIS.ALL_SCHOOL_TEMP_DATA2 WHERE LINCENSEE_ID = X.LINCENSEE_ID AND ROWNUM <= 1) SCHOOL_CODE FROM PSSPSIS.ALL_SCHOOL_TEMP_DATA2 X WHERE X.STS = 'A'").ToList();
                string ch = "123";
                var bridg = Db.ALL_SCHOOL_TEMP_DATA2.Where(x => x.STS == "A").ToList();
                foreach (var bri in bridg)
                {
                    if (bri.LINCENSEE_ID != ch)
                    {
                        var b = Db.ALL_SCHOOL_TEMP_DATA.FirstOrDefault(x => x.SCHOOL_CODE == bri.SCHOOL_CODE);
                        if (b != null)
                        {
                            var lic = new LICENSEE();
                            lic.APPLICANT_ID = "00";
                            lic.IS_CHAIN = 1;
                            lic.CHAIN_NAME =bri.LINCENSEE_ID ;
                            lic.LQUALIFICATION = b.OWNER_QUALIFICATION;
                            lic.LNAME = b.SCHOOL_OWNER;
                            lic.LCNIC = b.OWNER_CNIC;
                          //  lic.LTYPE = b.APPLICANT_TYPE;
                            lic.LMOBILENO = b.OWNER_MOBILE_NO;
                            lic.EMAIL = b.EMAIL;
                            lic.POSTADRS = b.POSTAL_ADDRESS;
                            lic.SEASON = b.SCHOOL_PHASE;
                            lic.OLD_APPLICATION_ID = b.APPLICATION_ID;
                            lic.BANK_NAME = b.BANK_NAME;
                            lic.ACCOUNT_TITLE = b.ACCOUNT_TITLE;
                            lic.BRANCH_CODE = b.BRANCH_CODE;
                            lic.ACCOUNT_NO = b.ACCOUNT_NO;
                            lic.ACCOUNT_TYPE = b.ACCOUNT_TYPE;
                            lic.PEF_MONITOR_NAME = b.PEF_MONITOR_NAME;
                            lic.PEF_MONITOR_CODE = b.PEF_MONITOR_CODE;
                            lic.PEFPROGRAMS = b.PEFPROGRAMS;
                            lic.PEF_ALL_SCHOOL_ID = b.PEF_ALL_SCHOOL_ID;
                            lic.PHASE_YEAR = b.PHASE_YEAR;

                           Db.LICENSEEs.Add(lic);
                            Db.SaveChanges();
                            processed++;
                        }
                        else
                        {
                            notfound += bri.LINCENSEE_ID + " - is not found </br>";

                        }
                        ch = bri.LINCENSEE_ID;
                    }
                 



                }

                return notfound + " </br>" + processed;
            }
            catch (DbEntityValidationException dbx)
            {
                string message =
                    dbx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors)
                        .Aggregate("",
                            (current, validationError) =>
                                current +
                                $"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage}");

                ModelState.AddModelError("", message);
                return message;
            }
            catch (Exception ex)
            {

                return ex.InnerException.Message;
            }
          
        }


        public string School()
        {

            int count = 0;
            var tempS = Db.ALL_SCHOOL_TEMP_DATA.ToList();
            foreach(var t in tempS)
            {
                var sc = new EMISS();
                sc.PKCODE = t.SCHOOL_CODE;
                sc.NAME = t.SCHOOL_NAME;
                sc.ADRS = t.SCHOOL_ADDRESS;
                sc.FKGEOLVL = Db.MUDDASIR_TEMP.FirstOrDefault(x=> x.SCHOOL_CODE == t.SCHOOL_CODE).FKGEOLVL;
                sc.FKUC = t.UC_NO;
                sc.PHNO = t.OFFICE_PHONE_NO;
                sc.LATITUDE = (t.LATITUDE!= null ? Convert.ToDecimal(t.LATITUDE) : 0);
                sc.LONGITUDE = (t.LONGITUDE != null ? Convert.ToDecimal(t.LONGITUDE) : 0);
                sc.PRINCIPLE_MOBILE = t.PRINCIPLE_MOBILE;
                sc.IS_BLACK_LISTED = "N";
                sc.ENABLE_FLAG = "Y";
                sc.CREATED_BY = "admin";
                sc.CREATION_DATE = DateTime.Now;
                sc.SCHOOL_LEVEL = t.SCHOOL_LEVEL;
                sc.SCHOOL_PHASE = t.SCHOOL_PHASE;
                Db.EMISSes.Add(sc);
                Db.SaveChanges();

                count++;


            }
            return "Enter School = "+count + "<br/> FOunded = "+tempS.Count;

        }

        public string Bridge()
        {
            
            int lisC = 0;
            int LicBr = 0;

            var licnse = Db.LICENSEEs.ToList();
            foreach(var l  in licnse)
            {
                List<ALL_SCHOOL_TEMP_DATA2> ls = new List<ALL_SCHOOL_TEMP_DATA2>();
                if(l.IS_CHAIN != 1)
                 ls = Db.ALL_SCHOOL_TEMP_DATA2.Where(x => x.LINCENSEE_ID == l.LCNIC).ToList();
                else
                {
                    ls = Db.ALL_SCHOOL_TEMP_DATA2.Where(x => x.LINCENSEE_ID == l.CHAIN_NAME).ToList();
                }
                   

                foreach(var bs in ls)
                {
                    var pb = new LICENSEE_BRIDGE();
                    pb.FKLICENSEE = l.ID;
                    pb.EMISCODE = bs.SCHOOL_CODE;
                    pb.ENABLE_FLAG = "Y";
                    pb.CREATION_DATE = DateTime.Now;
                    Db.LICENSEE_BRIDGE.Add(pb);
                    Db.SaveChanges();
                    LicBr++;
                }
                lisC++;

            }
            return "LicC = "+lisC+"</br> Lic Bridge = "+LicBr;
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }
    }
}