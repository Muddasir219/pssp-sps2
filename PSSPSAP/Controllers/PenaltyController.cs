﻿using PSSPSAP.Models;
using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class PenaltyController : BaseController
    {
        public PSSPSISEntities Db;
        public PenaltyController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: Penalty
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddThroughFile()
        {
            try
            {
                DataSet ds = new DataSet();
                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension =
                                         System.IO.Path.GetExtension(Request.Files["file"].FileName);

                    if (fileExtension != ".xls" && fileExtension != ".xlsx")
                        return Json(new { res = "Sorry You Select Wrong File" }, JsonRequestBehavior.AllowGet);

                    string fileLocation = "0";
                    string uploadDir = "~/PenaltySheet/";
                    //   string fileName = "";
                    System.IO.Directory.CreateDirectory(Server.MapPath(uploadDir));
                    var file = Request.Files["file"];
                    //Date Conversion Start

                    //Date Conversion End
                    var FileExtension = Path.GetExtension(file.FileName);
                    var FileName = "Penalty" + "_" + DateTime.UtcNow.AddHours(5).ToString("ddMMyyyyHHmmssffffff") + FileExtension;
                    fileLocation = Path.Combine(Server.MapPath(uploadDir), FileName);
                    file.SaveAs(fileLocation);
                    //File Saving End



                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }


                    var pcd = DateTime.Now.ToString("yy");
                    var mcd = DateTime.Now.Month.ToString();
                    var pcode = Convert.ToInt32(Request.Form["plenty"]);
                    var PenaltyMast = new PLANTIES_MAST()
                    {
                        FKPLANTIES = pcode,
                        FKTYPE = "11101",
                        FKMCD = mcd,
                        FKPCD = pcd,
                        ENABLE_FLAG = "Y",
                        CREATED_BY = GetUser().Id,
                        CREATION_DATE = DateTime.Now
                    };
                    Db.PLANTIES_MAST.Add(PenaltyMast);
                    Db.SaveChanges();

                    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                    {
                        var adjDtl = new PLANTIES_DTL();
                        adjDtl.FKPLANTY_MAST = PenaltyMast.ID;
                        adjDtl.FKPLANTY = pcode;
                        adjDtl.FKTYPE = "11101";
                        adjDtl.EMISCODE = ds.Tables[0].Rows[i][1].ToString();
                        adjDtl.FKMCD = mcd;
                        adjDtl.FKPCD = pcd;
                        adjDtl.ENABLE_FLAG = "Y";
                        adjDtl.CREATED_BY = GetUser().Id;
                        adjDtl.CREATION_DATE = DateTime.Now;
                        adjDtl.RMKS = "Excel Grouped";
                        Db.PLANTIES_DTL.Add(adjDtl);
                        Db.SaveChanges();


                    }
                }
                return Json(new { res = "apply Successfully", Err = "N" } , JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { res = "apply Successfully", Err = ex.InnerException==null ? ex.Message : ex.InnerException.Message } ,  JsonRequestBehavior.AllowGet);
            }

          
        }

        public JsonResult GetPnalties()
        {
            try
            {
                var ddl = Db.PLANTIES
                .Select(o => new { PKCODE = o.ID, Name = o.INDICATOR }).ToList();
                return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException == null ? ex.Message : ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ViewAllPenalies()
        {
            try
            {
                var ddl = Db.PLANTIES_MAST
                   .Select(o => new { PKCODE = o.ID, Name = o.PLANTy.INDICATOR , MCD = o.FKMCD,  PCD = o.FKPCD , Count = o.PLANTIES_DTL.Count }).ToList();
                return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException == null ? ex.Message : ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        

        [HttpPost]
        public JsonResult ApplyPenalty(PnaltyViewModel model)
        {
            try
            {
                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();

                var PenaltyMast = new PLANTIES_MAST()
                {
                    FKPLANTIES = model.Pnalety,
                    FKTYPE = model.FKTYPE,
                    FKMCD = mcd,
                    FKPCD = pcd,
                    ENABLE_FLAG = "Y",
                    CREATED_BY = GetUser().Id,
                    CREATION_DATE = DateTime.Now
                };
                Db.PLANTIES_MAST.Add(PenaltyMast);
                Db.SaveChanges();
                if (model.FKTYPE == "11101")
                {
                    List<string> dis = (model.Grop.Dist != null ? model.Grop.Dist.Select(x => x.PKCODE).ToList() : null);
                    List<string> phases = (model.Grop.Phase != null ? model.Grop.Phase.Select(x => x.PKCODE).ToList() : null);
                    List<decimal?> type = (model.Grop.appType != null ? model.Grop.appType.Select(x => x.PKCODE).ToList() : null);

                    var res = Db.V_TEMP_PAYMENT.AsQueryable();
                    if (dis != null && dis.Count>0)
                        res = res.Where(x => dis.Contains(x.DISTRICT));
                    if (phases != null && phases.Count > 0)
                        res = res.Where(x => phases.Contains(x.PHASE));
                    if (type != null && type.Count > 0)
                        res = res.Where(x => type.Contains(x.LTYPE));

                    var RES = res.ToList();
                    foreach (var e in RES)
                    {
                        var adjDtl = new PLANTIES_DTL();
                        adjDtl.FKPLANTY_MAST = PenaltyMast.ID;
                        adjDtl.FKPLANTY = model.Pnalety;
                        adjDtl.FKTYPE = model.FKTYPE;
                        adjDtl.EMISCODE = e.EMISCODE;
                        adjDtl.FKMCD = mcd;
                        adjDtl.FKPCD = pcd;
                        adjDtl.ENABLE_FLAG = "Y";
                        adjDtl.CREATED_BY = GetUser().Id;
                        adjDtl.CREATION_DATE = DateTime.Now;
                        adjDtl.RMKS = "Grouped";
                        Db.PLANTIES_DTL.Add(adjDtl);
                        Db.SaveChanges();
                    }
                }
                else if (model.FKTYPE == "11102")
                {
                    var adjDtl = new PLANTIES_DTL();
                    adjDtl.FKPLANTY_MAST = PenaltyMast.ID;
                    adjDtl.FKPLANTY = model.Pnalety;
                    adjDtl.FKTYPE = model.FKTYPE;
                    adjDtl.EMISCODE = model.EMIS;
                    adjDtl.FKMCD = mcd;
                    adjDtl.FKPCD = pcd;
                    adjDtl.ENABLE_FLAG = "Y";
                    adjDtl.CREATED_BY = GetUser().Id;
                    adjDtl.CREATION_DATE = DateTime.Now;
                    adjDtl.RMKS = "Individual";
                    Db.PLANTIES_DTL.Add(adjDtl);
                    Db.SaveChanges();
                }
                else
                {
                    var adjDtl = new PLANTIES_DTL();
                    adjDtl.FKPLANTY_MAST = PenaltyMast.ID;
                    adjDtl.FKPLANTY = model.Pnalety;
                    adjDtl.FKTYPE = model.FKTYPE;
                    adjDtl.RMKS = "All";
                    adjDtl.FKMCD = mcd;
                    adjDtl.FKPCD = pcd;
                    adjDtl.ENABLE_FLAG = "Y";
                    adjDtl.CREATED_BY = GetUser().Id;
                    adjDtl.CREATION_DATE = DateTime.Now;
                    Db.PLANTIES_DTL.Add(adjDtl);
                    Db.SaveChanges();

                }
                return Json(new { Ddl = "Pnalty apply Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException == null ? ex.Message : ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ApplyPenaltySelected(int pid , List<SelectedSchoolModel> model)
        {
            try
            {
                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();

                var PenaltyMast = new PLANTIES_MAST()
                {
                    FKPLANTIES = pid,
                    FKTYPE = "11101",
                    FKMCD = mcd,
                    FKPCD = pcd,
                    ENABLE_FLAG = "Y",
                    CREATED_BY = GetUser().Id,
                    CREATION_DATE = DateTime.Now
                };
                Db.PLANTIES_MAST.Add(PenaltyMast);
                Db.SaveChanges();
                foreach(var s in model)
                {
                    var adjDtl = new PLANTIES_DTL();
                    adjDtl.FKPLANTY_MAST = PenaltyMast.ID;
                    adjDtl.FKPLANTY = pid;
                    adjDtl.FKTYPE = "11101";
                    adjDtl.EMISCODE = s.emis;
                    adjDtl.FKMCD = mcd;
                    adjDtl.FKPCD = pcd;
                    adjDtl.ENABLE_FLAG = "Y";
                    adjDtl.CREATED_BY = GetUser().Id;
                    adjDtl.CREATION_DATE = DateTime.Now;
                    adjDtl.RMKS = "Grouped";
                    Db.PLANTIES_DTL.Add(adjDtl);
                    Db.SaveChanges();
                }
                return Json(new { res = "Apply Successfully ", Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException == null ? ex.Message : ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }
    }

   

    public class PnaltyViewModel
    {
        public string FKTYPE { get; set; }
        public SearchViewModel Grop { get; set; }
        public string EMIS { get; set; }       
        public int Pnalety { get; set; }
     

    }
    
    public class SelectedSchoolModel
    {
       public string emis { get; set; }
        public string name { get; set; }


    }
}