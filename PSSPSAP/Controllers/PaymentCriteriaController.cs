﻿using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    [Authorize (Roles = "admin")]

    public class PaymentCriteriaController : BaseController
    { 
       
        // GET: Common
        public PSSPSISEntities Db;
        public PaymentCriteriaController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: PaymentCriteria
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetRates(string pc , string mc)
        {
            try
            {
                if (String.IsNullOrEmpty(pc) && String.IsNullOrEmpty(mc))
                {
                    var Y = DateTime.Now.ToString("yy");
                    var M = DateTime.Now.Month.ToString();
                    var res = Db.PAYMENT_RATES.Where(x => x.FKPCD == Y && x.FKMCD == M)
                        .Select(x => new { ID = x.ID, RName = x.SPS_TYPE.NAME, AppType = x.APPLICENTTYPE.NAME, Rate = x.RATE, Month = x.FKMCD, Year = x.FKPCD })
                        .ToList().OrderBy(x=>x.AppType);
                    return Json(new { res = res, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var Y = pc;
                    var M = mc;
                    var res = Db.PAYMENT_RATES.Where(x => x.FKPCD == Y && x.FKMCD == M)
                        .Select(x => new { ID = x.ID, RName = x.SPS_TYPE.NAME, AppType = x.APPLICENTTYPE.NAME, Rate = x.RATE, Month = x.FKMCD, Year = x.FKPCD })
                        .ToList();
                    return Json(new { res = res, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveRates(PAYMENT_RATES model)
        {
           try
            {
                if (model.ID == 0)
                {
                    model.ENABLE_FLAG = "Y";
                    model.BHV = "+";
                    model.CREATED_BY = GetUser().Id;
                    model.CREATION_DATE = DateTime.Now;
                    var Y = DateTime.Now.ToString("yy");
                    var M = DateTime.Now.Month;
                    model.FKPCD = Y;
                    model.FKMCD = Convert.ToString(M);
                    Db.PAYMENT_RATES.Add(model);
                    Db.SaveChanges();
                    return Json(new { res = "Add Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var OldModel = Db.PAYMENT_RATES.FirstOrDefault(x => x.ID == model.ID);
                    OldModel.FKMCD = model.FKMCD;
                    OldModel.FKPCD = model.FKPCD;
                    OldModel.RATE = model.RATE;
                    OldModel.APPLICANT_TYPE = model.APPLICANT_TYPE;
                    OldModel.FKTYPE = model.FKTYPE;
                    Db.SaveChanges();
                    return Json(new { res = "Update Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult EditRate(int id)
        {
            try
            {
                Db.Configuration.ProxyCreationEnabled = false;
                var res = Db.PAYMENT_RATES.FirstOrDefault(x => x.ID == id);
                    
                return Json(new { res = res, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveRatesType(string id)
        {
            try
            {
                var rt = new SPS_TYPE();
                var LastType = Db.SPS_TYPE.Where(x => x.FKCODE == "10").Max(o => o.PKCODE);
                if (LastType != null)
                    rt.PKCODE = (Convert.ToInt32(LastType) + 1).ToString();
                else
                rt.PKCODE = "10100";
                rt.FKCODE = "10";
                rt.NAME = id;
                rt.LVL = "2";
                rt.ENABLE_FLAG = "Y";
                rt.LNTH = 5;
                rt.BHV = "+";
                rt.CREATED_BY = GetUser().Id;
                rt.CREATION_DATE = DateTime.Now;
                Db.SPS_TYPE.Add(rt);
                Db.SaveChanges();

                return Json(new { res = "Add Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPlanties()
        {
            try
            {
                Db.Configuration.ProxyCreationEnabled = false;
                var ddl = Db.PLANTIES.ToList();
                return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SavePlanty(PLANTy model)
        {
            try
            {
                string msg = "";
                if (model.ID == 0)
                {
                    model.ENABLE_FLAG = "Y";
                   
                    model.CREATED_BY = GetUser().Id;
                    model.CREATION_DATE = DateTime.Now;
                    Db.PLANTIES.Add(model);
                    Db.SaveChanges();
                    msg = "Add Successfully";


                }
                else
                {
                    var oldP = Db.PLANTIES.FirstOrDefault(x => x.ID == model.ID);
                    oldP.INDICATOR = model.INDICATOR;
                    oldP.PENALTY = model.PENALTY;
                    oldP.RMKS = model.RMKS;
                    oldP.NOOFSCHOOL = model.NOOFSCHOOL;
                    oldP.UPDATION_DATE = DateTime.Now;
                    oldP.CREATED_BY = GetUser().Id;
                    Db.SaveChanges();
                    msg = "Update Successfully";

                }

                Db.Configuration.ProxyCreationEnabled = false;
                var ddl = Db.PLANTIES.ToList();

                return Json(new { res =msg , panties = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { res = "", panties = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public JsonResult SaveRatesCategoryWise(PAYMENT_RATES model)
        {
            try
            {
                if (model.ID == 0)
                {
                    if(model.APPLICANT_TYPE==1)
                    {
                        var apptype = Db.APPLICENTTYPEs.Where(x => x.IS_CHAIN == "0").ToList();
                        if (apptype != null)
                        {
                            foreach (var data in apptype)
                            {
                                model.APPLICANT_TYPE = data.PKID;
                                model.ENABLE_FLAG = "Y";
                                model.BHV = "+";

                                model.CREATED_BY = GetUser().Id;
                                model.CREATION_DATE = DateTime.Now;
                                var Y = DateTime.Now.ToString("yy");
                                var M = DateTime.Now.Month;
                                model.FKPCD = Y;
                                model.FKMCD = Convert.ToString(M);
                                Db.PAYMENT_RATES.Add(model);
                                Db.SaveChanges();
                            }
                        }
                       
                    }


                    else if(model.APPLICANT_TYPE==2)
                    {
                        var apptype = Db.APPLICENTTYPEs.Where(x => x.IS_CHAIN == "1").ToList();
                        if (apptype != null)
                        {
                            foreach (var data in apptype)
                            {
                                model.APPLICANT_TYPE = data.PKID;
                                model.ENABLE_FLAG = "Y";
                                model.BHV = "+";

                                model.CREATED_BY = GetUser().Id;
                                model.CREATION_DATE = DateTime.Now;
                                var Y = DateTime.Now.Year.ToString("yy");
                                var M = DateTime.Now.Month;
                                model.FKPCD = Y;
                                model.FKMCD = Convert.ToString(M);
                                Db.PAYMENT_RATES.Add(model);
                                Db.SaveChanges();
                            }
                        }
                    }




                }
                return Json(new { res = "Add Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }
    }
}