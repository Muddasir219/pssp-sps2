﻿using PSSPSAP.Models;
using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class AdjustmentController : BaseController
    {
        // GET: Adjustment
        public PSSPSISEntities Db;
        public AdjustmentController()
        {
            Db = new PSSPSISEntities();
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SaveAdjustment(AdustmentViewModel model)
        {
            try
             {
                var adjMast = new ADJUSTMENT();
                adjMast.AMNT = model.AMNT;
                adjMast.NAME = model.NAME;
                adjMast.IAMNT = model.IAMNT;
                adjMast.FKTYPE = model.FKTYPE;
                adjMast.STEPS = model.STEPS;
                adjMast.FKMCD = DateTime.Now.AddMonths(model.STEPS).Month.ToString();
                adjMast.FKPCD = DateTime.Now.AddMonths(model.STEPS).ToString("yy"); 
                adjMast.ENABLE_FLAG = "Y";
                adjMast.CREATION_DATE = DateTime.Now;
                Db.ADJUSTMENTS.Add(adjMast);
                Db.SaveChanges();
                if(model.FKTYPE == "11101")
                {
                    List<string> dis = (model.Grop.Dist != null ? model.Grop.Dist.Select(x => x.PKCODE).ToList() : null);
                    List<string> phases = (model.Grop.Phase != null ? model.Grop.Phase.Select(x => x.PKCODE).ToList() : null);
                    List<decimal?> type = (model.Grop.appType != null ? model.Grop.appType.Select(x => x.PKCODE).ToList() : null);

                    var res = Db.V_LICENSEE_SCHOOLS.AsQueryable();
                    if (dis != null && dis.Count > 0)
                        res = res.Where(x => dis.Contains(x.DISTRICT));
                    if (phases != null && phases.Count > 0)
                        res = res.Where(x => phases.Contains(x.SCHOOL_PHASE));
                    if (type != null && type.Count > 0)
                        res = res.Where(x => type.Contains(x.LTYPE));

                    var RES = res.ToList();
                    foreach (var e in RES)
                    {
                        var adjDtl = new ADJ_EMIS_DTL();
                        adjDtl.FKADJ = adjMast.ID;
                        adjDtl.FKTYPE = model.FKTYPE;
                        adjDtl.EMISCODE = e.EMISCODE;
                        adjDtl.ENABLE_FLAG = "Y";
                        adjDtl.CREATED_BY = GetUser().Id;
                        adjDtl.CREATION_DATE = DateTime.Now;
                        adjDtl.RMKS = "Grouped";
                        Db.ADJ_EMIS_DTL.Add(adjDtl);
                        Db.SaveChanges();



                    }
                }
                else if(model.FKTYPE == "11102")
                {
                    var adjDtl = new ADJ_EMIS_DTL();
                    adjDtl.FKADJ = adjMast.ID;
                    adjDtl.FKTYPE = model.FKTYPE;
                    adjDtl.EMISCODE = model.EMIS;
                    adjDtl.ENABLE_FLAG = "Y";
                    adjDtl.CREATED_BY = GetUser().Id;
                    adjDtl.CREATION_DATE = DateTime.Now;
                    adjDtl.RMKS = "Individual";
                    Db.ADJ_EMIS_DTL.Add(adjDtl);
                    Db.SaveChanges();
                }
                else
                {
                    var adjDtl = new ADJ_EMIS_DTL();
                    adjDtl.FKADJ = adjMast.ID;
                    adjDtl.FKTYPE = model.FKTYPE;
                    adjDtl.RMKS = "All";
                    adjDtl.ENABLE_FLAG = "Y";
                    adjDtl.CREATED_BY = GetUser().Id;
                    adjDtl.CREATION_DATE = DateTime.Now;
                    Db.ADJ_EMIS_DTL.Add(adjDtl);
                    Db.SaveChanges();

                }



                return Json(new { Ddl = "Adjustment Apply Successfully", Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetAllAdjustment()
        {
            try
            {
                var res = Db.ADJUSTMENTS.Select(x => new { Name = x.NAME, Amount = x.AMNT, Instalment = x.IAMNT, Step = x.STEPS, PCD = x.FKPCD, MCD = x.FKMCD }).ToList();

                return Json(new { Ddl = res, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult VerifyEmis(string id)
        {
            try
            {
                string name = "";
                var res = Db.EMISSes.FirstOrDefault(x => x.PKCODE == id);
                if (res != null)
                    name = res.NAME;
                else
                    name = "0";
                 

                return Json(new { Ddl = name, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }
    }
    public class AdustmentViewModel
    {
        public string FKTYPE { get; set; }
        public SearchViewModel Grop { get; set; }
        public string EMIS { get; set; }
        public string NAME { get; set; }
        public int AMNT { get; set; }
        public int IAMNT { get; set; }
        public int STEPS { get; set; }
      
    }
}