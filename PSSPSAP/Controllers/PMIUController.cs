﻿using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace PSSPSAP.Controllers
{
    public class PMIUController : BaseController
    {
        public PSSPSISEntities Db;
        public PMIUController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: PMIU
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload()
        {
            try
            {

                ////////////////////////////////////
                DataSet ds = new DataSet();
                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension =
                                         System.IO.Path.GetExtension(Request.Files["file"].FileName);

                    if (fileExtension != ".xls" && fileExtension != ".xlsx")
                        return Json(new { res = "Sorry You Select Wrong File" }, JsonRequestBehavior.AllowGet);

                    string fileLocation = "0";
                    string uploadDir = "~/PMIU/";
                    //   string fileName = "";
                    System.IO.Directory.CreateDirectory(Server.MapPath(uploadDir));
                    var file = Request.Files["file"];
                    //Date Conversion Start
                    var date = Request.Form["date"];
                    var sd = date.Split(' ');
                    var ssd = sd[1] + "-" + sd[2] + "-" + sd[3] + " " + sd[4];
                    DateTime Udate = Convert.ToDateTime(ssd);
                    //Date Conversion End
                    var FileExtension = Path.GetExtension(file.FileName);
                    var FileName = "PMIU" + "_" + DateTime.UtcNow.AddHours(5).ToString("ddMMyyyyHHmmssffffff") + FileExtension;
                    fileLocation = Path.Combine(Server.MapPath(uploadDir), FileName);
                    file.SaveAs(fileLocation);
                    //File Saving End



                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }

                    //if (fileExtension.ToString().ToLower().Equals(".xml"))
                    //{
                    //    string fileLocation = Server.MapPath("~/Content/") + Request.Files["FileUpload"].FileName;
                    //    if (System.IO.File.Exists(fileLocation))
                    //    {
                    //        System.IO.File.Delete(fileLocation);
                    //    }

                    //    Request.Files["FileUpload"].SaveAs(fileLocation);
                    //    XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                    //    // DataSet ds = new DataSet();
                    //    ds.ReadXml(xmlreader);
                    //    xmlreader.Close();
                    //}

                    var mast = new PSSPREPMAST();
                    mast.FKMCD = Udate.Month.ToString();

                    var yyyy = Udate.Year.ToString().ToCharArray();
                    mast.FKPCD = "" + yyyy[2] + yyyy[3];
                    mast.ENABLED_FLAG = "PMIU";
                    mast.UPLOAD_BY = GetUser().Id;
                    mast.UPLOAD_DATE = Udate;
                    mast.FILE_NAME = FileName;
                    Db.PSSPREPMASTs.Add(mast);
                    Db.SaveChanges();
                    int trno = 1;
                    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                    {

                        var hom = new PSSPPMIU();
                        hom.SRNO = mast.SRNO;
                        hom.TRNO = trno;
                        hom.EMISCODE = ds.Tables[0].Rows[i][1].ToString();
                        hom.PHASE = ds.Tables[0].Rows[i][2].ToString();
                        hom.SCHOOL_NAME = ds.Tables[0].Rows[i][3].ToString();
                        hom.DISTRICT_NAME = ds.Tables[0].Rows[i][4].ToString();
                        hom.TEHSIL = ds.Tables[0].Rows[i][5].ToString();
                        hom.FKMCD = ds.Tables[0].Rows[i][6].ToString();
                        hom.FKPCD = ds.Tables[0].Rows[i][7].ToString();
                       
                        hom.STDNO = Convert.ToDecimal(ds.Tables[0].Rows[i][8]);
                        hom.STNO_PRASENT = Convert.ToDecimal(ds.Tables[0].Rows[i][9]);
                        hom.ABSENTISM_PER_AGE = Convert.ToDecimal(ds.Tables[0].Rows[i][10]);


                        Db.PSSPPMIUs.Add(hom);
                        Db.SaveChanges();
                        trno++;


                    }
                }


                return Json(new { res = "Uploaded" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { res = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult District()
        {
            var res = Db.PSSPPMIUs.Select(x => new { Name = x.DISTRICT_NAME }).Distinct().ToList();
            return Json(new { res = res }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetPMIUReport(string id)
        {
            Db.Configuration.ProxyCreationEnabled = false;
            var mid = Db.PSSPREPMASTs.Where(x=> x.ENABLED_FLAG=="PMIU").Max(x => x.SRNO);
            var res = Db.PSSPPMIUs.Where(x => x.DISTRICT_NAME == id && x.SRNO == mid).ToList();
            return Json(new { res = res }, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }
    }
}