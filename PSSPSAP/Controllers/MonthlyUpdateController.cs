﻿using PSSPSAP.Models;
using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class MonthlyUpdateController : BaseController
    {
        PSSPSISEntities Db;
        public MonthlyUpdateController()
        {
            Db = new PSSPSISEntities();
        }
        // GET: MonthlyUpdate
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult FindSchool(PaginationViewModel model)
        {
            try
            {
                List<string> dis = (model.Filters.Dist!=null ? model.Filters.Dist.Select(x => x.PKCODE).ToList() : new List<string>()) ;
                List<string> phases = (model.Filters.Phase != null ? model.Filters.Phase.Select(x => x.PKCODE).ToList() : new List<string>());
                List<decimal?> type = (model.Filters.appType != null ? model.Filters.appType.Select(x => x.PKCODE).ToList() : new List<decimal?>());

                var res = Db.V_MONTHLY_UPDATE_REP.AsQueryable();
                if(dis.Count>0)
                    res = res.Where(x => dis.Contains(x.DISTRICT));
                if (phases.Count > 0)
                    res = res.Where(x => phases.Contains(x.SCHOOL_PHASE));
                if (type.Count > 0)
                    res = res.Where(x => type.Contains(x.LTYPE));

                if (!string.IsNullOrEmpty(model.Search))
                {
                    res = res.Where(x => x.EMISCOD.ToLower().Contains(model.Search.ToLower())
                    || x.SCHOOL_NAME.ToLower().Contains(model.Search.ToLower())
                    || x.SCHOOL_LEVEL.ToLower().Contains(model.Search.ToLower())
                     || x.DISTRICT_NAME.ToLower().Contains(model.Search.ToLower())
                      || x.SCHOOL_PHASE.ToLower().Contains(model.Search.ToLower())
                       || x.LTYPE_NAME.ToLower().Contains(model.Search.ToLower())
                         || x.LNAME.ToLower().Contains(model.Search.ToLower())
                          
                    );
                }
                switch (model.SortByFieldName)
                {
                    case "EMISCOD":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.EMISCOD) : res.OrderBy(x => x.EMISCOD);
                        break;
                    case "SCHOOL_NAME":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.SCHOOL_NAME) : res.OrderBy(x => x.SCHOOL_NAME);
                        break;
                    case "SCHOOL_LEVEL":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.SCHOOL_LEVEL) : res.OrderBy(x => x.SCHOOL_LEVEL);
                        break;
                    case "DISTRICT_NAME":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.DISTRICT_NAME) : res.OrderBy(x => x.DISTRICT_NAME);
                        break;
                    case "SCHOOL_PHASE":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.SCHOOL_PHASE) : res.OrderBy(x => x.SCHOOL_PHASE);
                        break;
                    case "LTYPE_NAME":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.LTYPE_NAME) : res.OrderBy(x => x.LTYPE_NAME);
                        break;
                    case "LNAME":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.LNAME) : res.OrderBy(x => x.LNAME);
                        break;
                    case "TOTAL_STUDENT":
                        res = (model.SortBy == "desc") ? res.OrderByDescending(x => x.TOTAL_STUDENT) : res.OrderBy(x => x.TOTAL_STUDENT);
                        break;
                  

                    default:
                        res = res.OrderByDescending(x => x.EMISCOD);
                        break;
                }

                var totalCont = res.Count();
                var fList = res.Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();
               // var RES = res.ToList();
                return Json(new { res = fList, TotalCount = totalCont, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", TotalCount = 0, Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DistrictWiseSummery()
        {
            try
            {

                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();
              
                string sql = @"
 SELECT                                 A.DISTRICT,   
                                         A.DISTRICT_NAME,
              COUNT( A.EMISCODE  )       EMISCODE,
              SUM(   A.REPORT    )       MONTH_REPORT_UPDATED

          FROM (
             SELECT   SUBSTR(A.FKGEOLVL,1,8) DISTRICT,G.NAME DISTRICT_NAME,
                      A.PKCODE EMISCODE,(
                                         SELECT COUNT(EMIS) REPORT 
                                          FROM SWIMAST 
                                            WHERE   EMIS    =   A.PKCODE 
                                             AND    FKPCD   =  '"+pcd+@"' 
                                             AND    FKMCD   =   '"+mcd+@"'          ) REPORT   
            FROM EMISS A, GEOLVL G, LICENSEE_BRIDGE B 
                WHERE SUBSTR(A.FKGEOLVL,1,8) = G.PKCODE
                 AND     A.PKCODE       =   B.EMISCODE
            ) A
           
GROUP BY   A.DISTRICT,A.DISTRICT_NAME
";

               var r =  Db.Database.SqlQuery<MonthlyReportDisWise>(sql).ToList();

                return Json(new { Ddl = r,  Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult CatWiseSummery()
        {
            try
            {
                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();

                string sql = @" SELECT    A.LTYPE,   
                                         A.APPLICANT_NAME,
              COUNT( A.EMISCODE  )       EMISCODE_COUNT,
              SUM(   A.REPORT    )       MONTH_REPORT_UPDATED
       FROM (
             SELECT   NVL(C.LTYPE,999)LTYPE, NVL(E.NAME,'N/A') APPLICANT_NAME,
                      A.PKCODE EMISCODE,(
                                         SELECT COUNT(EMIS) REPORT 
                                          FROM SWIMAST 
                                            WHERE   EMIS    =   A.PKCODE 
                                             AND    FKPCD   =  '" + pcd + @"'
                                             AND    FKMCD   =   '" + mcd + @"'       ) REPORT   
            FROM EMISS A,  LICENSEE_BRIDGE B, LICENSEE C, APPLICENTTYPE E
                WHERE    A.PKCODE           =   B.EMISCODE 
                 AND     C.ID           (+) =   B.FKLICENSEE 
                 AND     E.PKID         (+) =   C.LTYPE 
            ) A
GROUP BY   A.LTYPE,A.APPLICANT_NAME";

                var r = Db.Database.SqlQuery<MonthlyReportCatWise>(sql).ToList();

                return Json(new { Ddl = r, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TypeWiseSummery()
        {
            try
            {
                //var Res = Db.EMISSes.Where(x => x.SWIMASTs.FirstOrDefault(o => o.FKMCD == "" && o.FKPCD == "") == null && x.GEOLVL == "").ToList();
                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();

                string sql = @"    SELECT    DECODE(A.IS_CHAIN,1,'IS CHAIN',0,'IS NOT A CHAIN',999,'NULL APPLICANT TYPE')IS_CHAIN,
              COUNT( A.EMISCODE  )       EMISCODE_COUNT,
              SUM(   A.REPORT    )       MONTH_REPORT_UPDATED
       FROM (
             SELECT   NVL(C.IS_CHAIN,'999')IS_CHAIN,
                      A.PKCODE EMISCODE,(
                                         SELECT COUNT(EMIS) REPORT 
                                          FROM SWIMAST 
                                            WHERE   EMIS    =   A.PKCODE 
                                             AND    FKPCD   =  '" + pcd + @"' 
                                             AND    FKMCD   = '" + mcd + @"'       ) REPORT   
            FROM EMISS A,  LICENSEE_BRIDGE B, LICENSEE C, APPLICENTTYPE E
                WHERE    A.PKCODE           =   B.EMISCODE 
                 AND     C.ID           (+) =   B.FKLICENSEE 
                 AND     E.PKID         (+) =   C.LTYPE 
            ) A
GROUP BY   A.IS_CHAIN";

                var r = Db.Database.SqlQuery<MonthlyReporttYPEWise>(sql).ToList();

                return Json(new { Ddl = r, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateGroup(BLACK_GROUP model , List<SelectedSchool> list )
        {
            using (var trans = Db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PKCODE == "0")
                    {

                        model.CREATED_BY = GetUser().Id;
                        model.CREATION_DATE = DateTime.Now;
                        model.ENABLE_FLAG = "Y";
                        Db.BLACK_GROUP.Add(model);
                        Db.SaveChanges();
                        foreach (var s in list )
                        {
                            var bgd = new BLACK_GROUP_DTL
                            {
                                FK_EMISCODE = s.emis,
                                FK_BLACK_GROUP = model.PKCODE,
                                ENABLE_FLAG = "Y",
                                CREATED_BY = GetUser().Id,
                                CREATION_DATE = DateTime.Now,
                            };
                            Db.BLACK_GROUP_DTL.Add(bgd);
                            Db.SaveChanges();
                        }
                    }
                    else
                    {
                        var bg = Db.BLACK_GROUP.FirstOrDefault(x => x.PKCODE == model.PKCODE);
                        bg.UPDATION_DATE = DateTime.Now;
                        bg.UPDTED_BY = GetUser().Id;
                        bg.NAME = model.NAME;
                        bg.EFACT = model.EFACT;
                        bg.PENALTY = model.PENALTY;

                        Db.SaveChanges();
                    }
                    trans.Commit();
                    return Json(new { res = "Save Successfully", Err = "N" });
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    return Json(new { res = "", Err = ex.InnerException != null ? ex.InnerException.Message : ex.Message });
                  
                }
            }
            
        }

        

        public JsonResult ViewGroup()
        {
            try
            {
                var RES = Db.BLACK_GROUP.Select(x => new { NAME = x.NAME, PENALTY = x.PENALTY, PKCODE = x.PKCODE, EFACT = x.EFACT, Count = x.BLACK_GROUP_DTL.Count }).ToList();
                return Json(new { res = RES, Err = "N" } , JsonRequestBehavior.AllowGet );
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException != null ? ex.InnerException.Message : ex.Message } , JsonRequestBehavior.AllowGet);
                throw;
            }
        }
        
        public JsonResult ShowBlakSchoolDtl(string id)
        {
            try
            {
                var RES = Db.BLACK_GROUP_DTL.Where(x=> x.ENABLE_FLAG=="Y").Select(x => new { NAME = x.EMISS.NAME, EMISCODE = x.FK_EMISCODE, PKCODE = x.PKCODE }).ToList();
                return Json(new { res = RES, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException != null ? ex.InnerException.Message : ex.Message }, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public JsonResult RemoveFromBlackList(string id)
        {
            try
            {
                var OBJ = Db.BLACK_GROUP_DTL.FirstOrDefault(x => x.PKCODE == id);
                OBJ.ENABLE_FLAG = "N";
                Db.SaveChanges();
                var RES = Db.BLACK_GROUP_DTL.Where(x => x.ENABLE_FLAG == "Y").Select(x => new { NAME = x.EMISS.NAME, EMISCODE = x.FK_EMISCODE, PKCODE = x.PKCODE }).ToList();
                return Json(new { res = RES, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException != null ? ex.InnerException.Message : ex.Message }, JsonRequestBehavior.AllowGet);
                throw;
            }
        }



        public JsonResult GetNotSubmittedSchool(string id , string Type)
        {
            try
            {
                var pcd = DateTime.Now.ToString("yy");
                var mcd = DateTime.Now.Month.ToString();
                if (Type == "N"){
                    var Res = Db.EMISSes.Where(x => !x.SWIMASTs.Any(o => o.FKMCD == mcd && o.FKPCD == pcd) && x.DISTRICT == id).
                        Select(x => new { x.NAME, Dis = x.GEOLVL.NAME, x.PKCODE, x.PHNO, x.SCHOOL_PHASE }).
                        ToList();
                    return Json(new { res = Res, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var Res = Db.EMISSes.Where(x => x.SWIMASTs.Any(o => o.FKMCD == mcd && o.FKPCD == pcd) && x.DISTRICT == id).
                                           Select(x => new { x.NAME, Dis = x.GEOLVL.NAME, x.PKCODE, x.PHNO,x.SCHOOL_PHASE}).
                                           ToList();
                    return Json(new { res = Res, Err = "N" }, JsonRequestBehavior.AllowGet);
                }

               
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException != null ? ex.InnerException.Message : ex.Message }, JsonRequestBehavior.AllowGet);
             
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }

    }

    public class MonthlyReportDisWise {

        public string DISTRICT { get; set; }
        public string DISTRICT_NAME { get; set; }
        public decimal? EMISCODE { get; set; }
        public decimal? MONTH_REPORT_UPDATED { get; set; }
    }

    public class SelectedSchool
    {
        public string emis { get; set; }
        public string name { get; set; }
    }
    public class MonthlyReportCatWise
    {

        public decimal LTYPE { get; set; }
        public string APPLICANT_NAME { get; set; }
        public decimal? EMISCODE_COUNT { get; set; }
        public decimal? MONTH_REPORT_UPDATED { get; set; }
    }
    public class MonthlyReporttYPEWise
    {

      
        public string IS_CHAIN { get; set; }
        public decimal? EMISCODE_COUNT { get; set; }
        public decimal? MONTH_REPORT_UPDATED { get; set; }
    }
}