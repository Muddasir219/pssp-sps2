﻿using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class PaymentHistoryController : Controller
    {
        public PSSPSISEntities Db = new PSSPSISEntities();
        // GET: PaymentHistory
        public ActionResult Index()
        {
            
            return View();
        }



        [HttpPost]
        public JsonResult GetPayment(string pc, string mc)
        {
            try
            {
               
                if (String.IsNullOrEmpty(pc) && String.IsNullOrEmpty(mc))
                {
                    var Y = DateTime.Now.ToString("yy");
                    var M = DateTime.Now.Month.ToString();

                    var res = Db.PAYMENTS.Where(x => x.FKPCD == Y && x.FKMCD == M)
                        .Select(x => new {SName=x.EMISS.NAME,Phase=x.EMISS.SCHOOL_PHASE, Emiscode = x.EMISCODE, Month = x.FKMCD, Year = x.FKPCD,District=x.EMISS.GEOLVL.NAME,PEnroll=x.PRIMARY_ENROL,PEnrollPaid=x.PRIMARY_ENROL_PAID,PrimaryPymt=x.PRIMARY_PAYMENT,Mangmentfee=x.MANAGEMENT_FEE,Gross=x.GROSS,Detuction=x.DEDUCTIONS,ReleasePymt=x.RELEASE_PAYMENT,netpymt=x.NET_PAYMENT })
                        .ToList();

                    return Json(new { res = res, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var Y = pc;
                    var M = mc;
                    var res = Db.PAYMENTS.Where(x => x.FKPCD == Y && x.FKMCD == M)
                        .Select(x => new { SName = x.EMISS.NAME, Phase = x.EMISS.SCHOOL_PHASE, Emiscode = x.EMISCODE, Month = x.FKMCD, Year = x.FKPCD ,District = x.EMISS.GEOLVL.NAME, PEnroll = x.PRIMARY_ENROL, PEnrollPaid = x.PRIMARY_ENROL_PAID, PrimaryPymt = x.PRIMARY_PAYMENT, Mangmentfee=x.MANAGEMENT_FEE, Gross = x.GROSS, Detuction = x.DEDUCTIONS, ReleasePymt = x.RELEASE_PAYMENT, netpymt = x.NET_PAYMENT })
                        .ToList();
                    return Json(new { res = res, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { res = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}