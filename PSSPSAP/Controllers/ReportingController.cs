﻿using PSSPSAP.Models;
using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class ReportingController : BaseController
    {
        PSSPSISEntities Db = new PSSPSISEntities();
        // GET: Reporting By Rizwan
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetPayments(PaginationViewModel model)
        {

            var res = Db.PAYMENTS.AsQueryable();

            if (!string.IsNullOrEmpty(model.Search))
            {
                res = res.Where(x => x.EMISCODE.ToLower().Contains(model.Search.ToLower())
                || x.EMISS.NAME.ToLower().Contains(model.Search.ToLower())
                || x.EMISS.GEOLVL.NAME.ToLower().Contains(model.Search.ToLower())
                 || x.EMISS.GEOLVL1.NAME.ToLower().Contains(model.Search.ToLower())
                );
            }
            var list = res.Select(x => new {
                SNAME=x.EMISS.NAME,
                Dis = x.EMISS.GEOLVL.NAME,
                Teh = x.EMISS.GEOLVL1.NAME,
                fkpcd = x.FKPCD,
                fkmcd = x.FKMCD,
                EmisCode = x.EMISCODE,
                FkGeolvl = x.FKGEOLVL,
                PrimaryEnrol = x.PRIMARY_ENROL,
                PrimaryEnrol_s = x.PRIMARY_ENROL_S,
                PrimaryEnrolPaid = x.PRIMARY_ENROL_PAID,
                Ppayment = x.PRIMARY_PAYMENT,
                MngmntFee = x.MANAGEMENT_FEE,
                Gross = x.GROSS,
                Stoppaymnt = x.STOP_PAYMENT,
                Deduction = x.DEDUCTIONS,
                ReleasePayment = x.RELEASE_PAYMENT,
                NetPayment = x.NET_PAYMENT,
                AppType = x.APPLICANT_TYPE,
                Remarks = x.RMKS,
                OtherPayment = x.OTHER_PAYMENT,
                penalityRmks = x.PLANTY_RMKS,
                PDescription = x.DEDUCTIONS,
                SecondaryEnrol = x.SECONDARY_ENROL,
                SecondaryEnrolPaid = x.SECONDARY_ENROL_PAID,
                Spayment = x.SECONDARY_PAYMENT
            }).OrderBy(x => x.EmisCode).Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();

            var totalCont = res.Count();
            return Json(new { res = list, TotalCount = totalCont, Err = "N" }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Download()
        {
            var stream = new PaymentDetailReport().Create();

            return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            //return  File(stream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document","Yoo.docx");
            //return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");

        }
    }
}