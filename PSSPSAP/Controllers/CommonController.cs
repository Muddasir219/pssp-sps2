﻿using PSSPSAP.Models.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSSPSAP.Controllers
{
    public class CommonController : BaseController
    {
        // GET: Common
        public PSSPSISEntities Db;
        public CommonController()
        {
            Db = new PSSPSISEntities();
        }
        public JsonResult DropDistrict()
        {
            try
            {
                if (!User.IsInRole("Focal"))
                {
                    var ddl = Db.GEOLVLs.Where(x => x.LVL == "District" && x.FKCODE.StartsWith("06"))
                    .Select(o => new { PKCODE = o.PKCODE, Name = o.NAME }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var glvl = GetUser().Address;
                    var ddl = Db.GEOLVLs.Where(x=> x.PKCODE == glvl )
                  .Select(o => new { PKCODE = o.PKCODE, Name = o.NAME }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(new { Ddl = "" , Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFocal()
        {
            try
            {
                if (User.IsInRole("admin"))
                {
                    var ddl = Db.AspNetUsers.Where(x => x.Rmks.Contains("F"))
                    .Select(o => new { PKCODE = o.Id, Name = o.UserName }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uid = GetUser().Id;
                    var ddl = Db.AspNetUsers.Where(x => x.Id == uid)
                        .Select(o => new { PKCODE = o.Id, Name = o.UserName }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DropAppTypes()
        {
            try
            {
                if (User.IsInRole("admin"))
                {
                    var ddl = Db.APPLICENTTYPEs.Select(x => new { PKCODE = x.PKID, Name = x.NAME, isC = x.IS_CHAIN }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var typid = Convert.ToInt32(GetUser().UserDtl);
                    var ddl = Db.APPLICENTTYPEs.Where(x => x.PKID == typid) .Select(x => new { PKCODE = x.PKID, Name = x.NAME, isC = x.IS_CHAIN }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRatesType(string id)
        {
            try
            {
                var od = DateTime.Now.AddDays(15);
                var d = od.Subtract(DateTime.Now);
                int h =(int) d.TotalHours;
                if (id == "1")
                {
                    var ddl = Db.SPS_TYPE.Where(o => o.FKCODE == "10").Select(x => new { PKCODE = x.PKCODE, Name = x.NAME }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var ddl = Db.SPS_TYPE.Where(o => o.FKCODE == "10" && o.PKCODE!= "10101").Select(x => new { PKCODE = x.PKCODE, Name = x.NAME }).ToList();
                    return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult DropPCDMCD()
        {
            try
            {
                var pcd = Db.PSSPPCDs.Select(x => new { PKCODE = x.PKCODE, Name = x.NAME }).ToList();
                var mcd = Db.PSSPMCDs.Select(x => new { PKCODE = x.PKCODE, Name = x.NAME }).ToList();
                return Json(new { PCD = pcd , MCD = mcd, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { PCD = "", MCD = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AdjustmrntTypes()
        {
            try
            {
                var ddl = Db.SPS_TYPE.Where(o => o.FKCODE == "11").Select(x => new { PKCODE = x.PKCODE, Name = x.NAME }).ToList();
                return Json(new { Ddl = ddl, Err = "N" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Ddl = "", Err = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Db.Dispose();
        }
        
    }
}