//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSSPSAP.Models.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class PLANTIES_MAST
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PLANTIES_MAST()
        {
            this.PLANTIES_DTL = new HashSet<PLANTIES_DTL>();
        }
    
        public decimal ID { get; set; }
        public string FKPCD { get; set; }
        public string FKMCD { get; set; }
        public string FKTYPE { get; set; }
        public Nullable<decimal> FKPLANTIES { get; set; }
        public string RMKS { get; set; }
        public string ENABLE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public string UPDTED_BY { get; set; }
        public Nullable<System.DateTime> UPDATION_DATE { get; set; }
    
        public virtual PLANTy PLANTy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLANTIES_DTL> PLANTIES_DTL { get; set; }
        public virtual PSSPMCD PSSPMCD { get; set; }
        public virtual PSSPPCD PSSPPCD { get; set; }
        public virtual SPS_TYPE SPS_TYPE { get; set; }
    }
}
