//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSSPSAP.Models.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TIS_TRAINING
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TIS_TRAINING()
        {
            this.TIS_TRAINING_DTL = new HashSet<TIS_TRAINING_DTL>();
        }
    
        public decimal ID { get; set; }
        public string NAME { get; set; }
        public string DETAIL { get; set; }
        public Nullable<System.DateTime> TRAINING_STTIME { get; set; }
        public Nullable<System.DateTime> TRAINING_ENTIME { get; set; }
        public string RMKS { get; set; }
        public string ENABLE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public string UPDTED_BY { get; set; }
        public Nullable<System.DateTime> UPDATION_DATE { get; set; }
        public Nullable<decimal> TEACHER_LIMIT { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TIS_TRAINING_DTL> TIS_TRAINING_DTL { get; set; }
    }
}
