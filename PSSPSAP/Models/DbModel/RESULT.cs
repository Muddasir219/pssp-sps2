//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSSPSAP.Models.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class RESULT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RESULT()
        {
            this.RESULTS_DOC = new HashSet<RESULTS_DOC>();
            this.RESULTS_DTL = new HashSet<RESULTS_DTL>();
        }
    
        public decimal PKCODE { get; set; }
        public string NAME { get; set; }
        public string FKPCD { get; set; }
        public string STEPS { get; set; }
        public string DETAIL { get; set; }
        public string RMKS { get; set; }
        public string ENABLE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public string UPDTED_BY { get; set; }
        public Nullable<System.DateTime> UPDATION_DATE { get; set; }
    
        public virtual PSSPPCD PSSPPCD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RESULTS_DOC> RESULTS_DOC { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RESULTS_DTL> RESULTS_DTL { get; set; }
    }
}
