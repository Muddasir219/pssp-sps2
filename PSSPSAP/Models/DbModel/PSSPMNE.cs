//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSSPSAP.Models.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSSPMNE
    {
        public decimal SRNO { get; set; }
        public decimal TRNO { get; set; }
        public decimal FKSCHOOL_HEADER { get; set; }
        public string EMISCODE { get; set; }
        public string PHASE { get; set; }
        public string SCHOOL_NAME { get; set; }
        public string DISTRICT_NAME { get; set; }
        public string TEHSIL { get; set; }
        public string FKMCD { get; set; }
        public string FKPCD { get; set; }
        public Nullable<decimal> STDINPMIU { get; set; }
        public Nullable<decimal> STDINREG { get; set; }
        public string ENABLED_FLAG { get; set; }
        public string CREATE_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATION_DATE { get; set; }
        public Nullable<decimal> STU_PRESENT { get; set; }
        public Nullable<decimal> ABSENTISMPERAGE { get; set; }
    
        public virtual PSSPMCD PSSPMCD { get; set; }
        public virtual PSSPPCD PSSPPCD { get; set; }
        public virtual PSSPREPMAST PSSPREPMAST { get; set; }
    }
}
