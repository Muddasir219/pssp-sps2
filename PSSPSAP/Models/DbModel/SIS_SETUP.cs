//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSSPSAP.Models.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SIS_SETUP
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SIS_SETUP()
        {
            this.BOOKS_DEMAND = new HashSet<BOOKS_DEMAND>();
            this.FEEDS = new HashSet<FEED>();
            this.FEEDS1 = new HashSet<FEED>();
            this.RESULTS_DOC = new HashSet<RESULTS_DOC>();
            this.STUDENT_INFO = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO1 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO2 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO3 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO4 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO5 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO6 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO7 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO8 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO9 = new HashSet<STUDENT_INFO>();
            this.STUDENT_INFO10 = new HashSet<STUDENT_INFO>();
            this.SIS_SETUP1 = new HashSet<SIS_SETUP>();
            this.SIS_SETUP11 = new HashSet<SIS_SETUP>();
            this.STUDENT_INFO11 = new HashSet<STUDENT_INFO>();
            this.SWIDTLs = new HashSet<SWIDTL>();
            this.STUDENT_INFO101 = new HashSet<STUDENT_INFO>();
        }
    
        public string PKCODE { get; set; }
        public string FKCODE { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string LVL { get; set; }
        public Nullable<long> LNTH { get; set; }
        public string REMARKS { get; set; }
        public string ENABLE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public string UPDTED_BY { get; set; }
        public Nullable<System.DateTime> UPDATION_DATE { get; set; }
        public Nullable<decimal> SORT { get; set; }
        public string FLAG { get; set; }
        public string FK_SIS_SETUP { get; set; }
        public string DIST_FLAG { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BOOKS_DEMAND> BOOKS_DEMAND { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FEED> FEEDS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FEED> FEEDS1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RESULTS_DOC> RESULTS_DOC { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO4 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO5 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO6 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO7 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO8 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO9 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO10 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SIS_SETUP> SIS_SETUP1 { get; set; }
        public virtual SIS_SETUP SIS_SETUP2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SIS_SETUP> SIS_SETUP11 { get; set; }
        public virtual SIS_SETUP SIS_SETUP3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO11 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SWIDTL> SWIDTLs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDENT_INFO> STUDENT_INFO101 { get; set; }
    }
}
