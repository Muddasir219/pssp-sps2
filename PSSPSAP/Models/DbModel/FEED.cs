//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSSPSAP.Models.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class FEED
    {
        public decimal ID { get; set; }
        public string FKQUEST { get; set; }
        public string FKANS { get; set; }
        public string REMARKS { get; set; }
        public string ENABLE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public string UPDTED_BY { get; set; }
        public Nullable<System.DateTime> UPDATION_DATE { get; set; }
    
        public virtual SIS_SETUP SIS_SETUP { get; set; }
        public virtual SIS_SETUP SIS_SETUP1 { get; set; }
    }
}
