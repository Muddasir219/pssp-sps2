﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSSPSAP.Models
{
    public class PaginationViewModel
    {

        public PaginationViewModel()
        {
            Filters = new SearchViewModel();
        }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public string Search { get; set; }
        public string SortBy { get; set; }
        public string SortByFieldName { get; set; }
        public int SortIndex { get; set; } = -1;
        public bool ShowRevoked { get; set; }
        public string MeetingAgendaId { get; set; }
        public string RequestFromModule { get; set; }
        public string pkcode { get; set; }
        public SearchViewModel Filters { get; set; }
    }

    public class SearchViewModel
    {

        public SearchViewModel()
        {
            Dist = new List<SearchItemViewModel>();
            appType = new List<AppTypeViewModel>();
            Phase = new List<SearchItemViewModel>();
        }
        public List<SearchItemViewModel> Dist { get; set; }
        public List<AppTypeViewModel> appType { get; set; }
        public List<SearchItemViewModel> Phase { get; set; }
        public List<SearchItemViewModel> Focal { get; set; }
    }

  
    public class SearchItemViewModel
    {
        public string Name { get; set; }
        public string PKCODE { get; set; }
    }
    public class AppTypeViewModel
    {
        public string Name { get; set; }
        public decimal? PKCODE { get; set; }
    }
}       