﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PSSPSAP.Startup))]
namespace PSSPSAP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
