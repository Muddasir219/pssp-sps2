﻿angular.module("SAP").requires.push("ngMaterial");
angular.module("SAP").requires.push('ngSanitize');
angular.module("SAP").requires.push("ui.select");

app.controller('Pnalety', ['$scope', '$http', function ($scope, $http) {
    $scope.alert = 0;
    $scope.alertMsg = '';

    $scope.loder = false;

   
    $scope.pnelty = '';
    $scope.file = null;

    //Drop District
    $http({
        url: "/Common/DropDistrict",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.District = res.data.Ddl;
    });
    //Drop Phase
    $scope.Phase = [{ PKCODE: 1, Name: "Phase 1" }, { PKCODE: 2, Name: "Phase 2" }, { PKCODE: 3, Name: "Phase 3" }];

    //Drop Applicent Types
    $http({
        url: "/Common/DropAppTypes",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.AppTypes = res.data.Ddl;
    });
    //Drop AdjustmrntTypes
    $http({
        url: "/Common/AdjustmrntTypes",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.AdjsTypes = res.data.Ddl;
    });
    //Drop Pnalties
    $http({
        url: "/Penalty/GetPnalties",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.planty = res.data.Ddl;
    });

    
    $scope.SelectedDis = [];
    $scope.SelectedPhase = [];
    $scope.SelectedType = [];

    $scope.PnaletyModel = {
        FKTYPE: "",
        Grop: {},
        EMIS: "",
        Pnalety: 0
    }
    $scope.ApplyPenalty = function () {
        $scope.Search = {
            Dist: $scope.SelectedDis,
            Phase: $scope.SelectedPhase,
            appType: $scope.SelectedType
        }
        $scope.PnaletyModel.Grop = $scope.Search;
        
        
        $http({
            url: "/Penalty/ApplyPenalty",
            method: "POST",
            data: $scope.PnaletyModel

        }).then(function (res) {
            if (res.data.Err == "N") {
                alert("Pnalety is apply successfully");
                $scope.DefineRates = null;
                location.reload();
            }
            else {
                alert(res.data.Err);
            }
        })
    }


    $scope.VerifyEmis = function (e) {

        $http({
            url: "/Adjustment/VerifyEmis/" + e,
            method: "GET"

        }).then(function (res) {

            $scope.SchoolName = res.data.Ddl;
        });
    }


    $scope.ViewAllPenalies = function () {
        //Get All Adjustment 
       
        $http({
            url: "/Penalty/ViewAllPenalies",
            method: "GET"

        }).then(function (res) {
            
            $scope.AppliedPlenties = res.data.Ddl;
        });
    }

    $scope.Save = function () {
        var fd = new FormData();
       
        fd.append("plenty", $scope.pnelty);
        fd.append("file", $scope.file);
        $http({
            url: "/Penalty/AddThroughFile",
            method: "POST",
            data: fd,
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (res) {

            if (res.data.Err == "N") {
                alert("Penalty applied Successfully")
            }
            else
            {
                alert(res.data.Err)
                console.log(res.data.Err)
            }
        });
    };
   

    //Alert Dispose
    $scope.disposAlert = function () {
        $scope.alert = 0;
        $scope.alertMsg = '';
    }

}]);

app.directive("fileInput", function () {
    return {

        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var file = elem[0].files[0];
                debugger;
                ngModel.$setViewValue(file);
            });
        }
    }
});
