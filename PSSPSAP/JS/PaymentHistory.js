﻿angular.module("SAP").requires.push("ngMaterial");
angular.module("SAP").requires.push('ngSanitize');
angular.module("SAP").requires.push("ui.select");
angular.module("SAP").requires.push("ui.bootstrap");


app.controller('PaymentHistory', ['$scope', '$http', function ($scope, $http) {

    //PCD and MCD 
    $http({
        url: "/Common/DropPCDMCD",
        method: "GET"

    }).then(function (res) {
        if (res.data.Err == "N") {

            $scope.PCD = res.data.PCD;
            $scope.MCD = res.data.MCD;
        }
        else {
            alert(res.data.Err);
        }
    });

    $scope.Payments = {
       // NAME: "",
        Emiscode:"",
        Month: "",
        Year: "",
        District: "",
        Phase:"",
        SName: "",
        PEnroll:"",
        PEnrollPaid:"",
        PrimaryPymt: "",
        Mangmentfee:"",
        Gross:"",
        Detuction:"",
        ReleasePymt:""
        ,netpymt:""
        

    }

    $scope.FKPCD = '';
    $scope.FKMCD = '';
    $scope.GetPayment = function () {
        $http({
            url: "/PaymentHistory/GetPayment",
            method: "POST",
            data: { pc: $scope.FKPCD, mc: $scope.FKMCD }

        }).then(function (res) {
            if (res.data.Err == "N") {

                $scope.Payments = res.data.res;
            }
            else {
                alert(res.data.Err);
            }
        });
    }

}]);