﻿angular.module("SAP").requires.push("ngMaterial");
//M&E controller
app.controller('ME', ['$scope', '$http', function ($scope, $http) {

    $scope.alert = 0;
    $scope.alertMsg = '';
    $scope.file = null;
    $scope.date = new Date();
    $http({
        url: "/MEReport/District",
        method: "GET"      
       
    }).then(function (res) {
        $scope.District = res.data.res;
    });


    //Upload function
    $scope.Upload = function () {
        console.log($scope.file);
        console.log($scope.date);
        $scope.alertMsg = "Please Wait ... ";
        $scope.alert = 2;
        var fd = new FormData();
        fd.append("file", $scope.file);
        fd.append("date", $scope.date);
        $http({
            url: "/MEReport/Upload",
            method: "POST",
            data: fd,
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (res) {
            $scope.alert = 0;
            $scope.alertMsg = res.data.res;
            $scope.alert = 1;
        });
    }
    //Alert Dispose
    $scope.disposAlert = function () {
        $scope.alert = 0;
        $scope.alertMsg = '';
    }

    //Get district wise Report

    $scope.GetMEReport = function (e) {
        $http({
            url: "/MEReport/GetMEReport/" + e,
            method: "GET"

        }).then(function (res) {
            console.log(res.data.res);
            $scope.MEReport = res.data.res;
        });
    }



}]);


//File Upload directive
app.directive("fileInput", function () {
    //File Upload directive
    return {

        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var file = elem[0].files[0];
                debugger;
                ngModel.$setViewValue(file);

               
            });
        }
    }
});