﻿angular.module("SAP").requires.push("ngMaterial");
angular.module("SAP").requires.push('ngSanitize');
angular.module("SAP").requires.push("ui.select");
angular.module("SAP").requires.push("ui.bootstrap");
app.controller('PaymentCalculation', ['$scope', '$http', function ($scope, $http) {
    //Alert
    $scope.alert = 0;
    $scope.alertMsg = '';
    //Alert Dispose
    $scope.disposAlert = function () {
        $scope.alert = 0;
        $scope.alertMsg = '';
    }
    ////////////////////

    //Drop District
    $http({
        url: "/Common/DropDistrict",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl);
        $scope.District = res.data.Ddl;
    });
    //Drop Phase
    $scope.Phase = [{ PKCODE: 1, Name: "Phase 1" }, { PKCODE: 2, Name: "Phase 2" }, { PKCODE: 3, Name: "Phase 3" }];


    //Drop Applicent Types
    $http({
        url: "/Common/DropAppTypes",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl);
        $scope.AppTypes = res.data.Ddl;
    });

    //Drop Applicent Types
    $http({
        url: "/Common/GetFocal",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl);
        $scope.FocalPersons = res.data.Ddl;
    });
    
    $scope.SelectedDis = [];
    $scope.SelectedPhase = [];
    $scope.SelectedType = [];
    $scope.SelectedFocalPersons = [];
    $scope.Pagination = { Page: 1, PageSize: 10, TotalRecords: 0, Search: "", SortByFieldName: "", SortBy: 'asc', Filters: {} };



    //FInd School
    $scope.FindSchool = function () {
        $scope.Search = {
            Dist: $scope.SelectedDis,
            Phase: $scope.SelectedPhase,
            appType: $scope.SelectedType,
            Focal : $scope.SelectedFocalPersons
        }
        $scope.Pagination.Filters = $scope.Search;
        console.log($scope.Pagination);



        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/PaymentCalculation/FindSchool",
            method: "POST",
            data: $scope.Pagination

        }).then(function (res) {
            console.log(res.data.res);
            $scope.Schools = res.data.res;
            $scope.Pagination.TotalRecords = res.data.TotalCount
            $scope.alertMsg = '';
            $scope.alert = 0;
        });
    }


    $scope.pageChanged = function () {
        $scope.FindSchool();
    }



    //Payment Calculation Summery
    $scope.popup = 0;
    $scope.ShowPopUp = function (e, index) {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;

        $scope.CSIndex = index;
        $http({
            url: "/PaymentCalculation/Calculate/",
            method: "POST",
            data: { emis: e}

        }).then(function (res) {
            if (res.data.Err == 'N') {
                console.log(res)
               
                $scope.Payment = res.data.pay;
                $scope.DTL = res.data.Pdtl;
                $scope.BR = res.data.BR;
                $scope.BRS = res.data.BRS;
                $scope.MC = res.data.MC;
                $scope.Pnalty = res.data.Pnalty;
                $scope.alertMsg = '';
              
                $scope.alert = 0;
                $scope.popup = 1;
            }
            else
            {
                $scope.alertMsg = res.data.Err;
            }
        });
       

    }
   
    //Update Payment Calculation Based on Student Qty
    $scope.ChangeStuQty = function (emis, stuQty) {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/PaymentCalculation/Calculate/",
            method: "POST",
            data: { emis: emis , StuQty : stuQty }

        }).then(function (res) {
            if (res.data.Err == 'N') {
                
                $scope.Payment = res.data.pay;
                $scope.DTL = res.data.Pdtl;
                $scope.BR = res.data.BR;
                $scope.MC = res.data.MC;
                $scope.Pnalty = res.data.Pnalty;
                $scope.alertMsg = '';
                $scope.alert = 0;
                $scope.popup = 1;
            }
            else {
                $scope.alertMsg = res.data.Err;
            }
        });
    }

    $scope.Stop = function (sr , srno) {
        $http({
            url: "/PaymentCalculation/StopRelease/",
            method: "POST",
            data: { SR: sr, SRNO: srno }

        }).then(function (res) {
            if (res.data.Err == 'N') {

               
                $scope.alertMsg = res.data.res;
                $scope.alert = 1;
                $scope.popup = 1;
            }
            else {
                $scope.alertMsg = res.data.Err;
            }
        });
    }
    $scope.ok = 1;
    //Calculate Payments For All
    $scope.CalculateAll = function () {
        $scope.Search = {
            Dist: $scope.SelectedDis,
            Phase: $scope.SelectedPhase,
            appType: $scope.SelectedType
        }
        console.log($scope.Search);
        $scope.alertMsg = 'Please Wait . . .';
        $scope.ok = 0;
        $scope.alert = 1;
        $http({
            url: "/PaymentCalculation/CalculateAllPayments",
            method: "POST",
            data: $scope.Search

        }).then(function (res) {
            console.log(res.data.res);
            $scope.alertMsg = "Calculate Successfully"
            $scope.ok = 1;
        });
    }

    //GetPaymentSheet in pdf formate
    $scope.GetPaymentSheet = function () {
        $scope.Search = {
            Dist: $scope.SelectedDis,
            Phase: $scope.SelectedPhase,
            appType: $scope.SelectedType
        }
        $http({
            url: "/PaymentSheet/index",
            method: "POST",
            data: $scope.Search

        }).then(function (res) {
            console.log(res.data);
            var w = window.open();
            var html = res.data
            w.document.write(html);
        });
    }

    $scope.DisposPopUp = function () {
        $scope.popup = 0;
    }


}]);





