﻿angular.module("SAP").requires.push("ngMaterial");
app.controller('PMIU', [ '$scope', '$http', function ($scope, $http) {

    console.log("anguler ok");
    
    $scope.file = null;
    $scope.date = new Date();
    $scope.alert = 0;
    $scope.alertMsg = '';

    $http({
        url: "/PMIU/District",
        method: "GET"

    }).then(function (res) {
        $scope.District = res.data.res;
    });


    $scope.Upload = function () {
        console.log($scope.file);
        console.log($scope.date);
        $scope.alertMsg = "Please Wait ... ";
        $scope.alert = 2;

        var fd = new FormData();
        fd.append("file", $scope.file);
        fd.append("date", $scope.date);     
        $http({
            url: "/PMIU/Upload",
            method: "POST",
            data: fd,
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (res) {
            $scope.alert = 0;
            $scope.alertMsg = res.data.res;
            $scope.alert = 1;
        });
    }

    //Alert Dispose
    $scope.disposAlert = function () {
        $scope.alert = 0;
        $scope.alertMsg = '';
    }



    //Get district wise Report

    $scope.GetMEReport = function (e) {
        $http({
            url: "/PMIU/GetPMIUReport/" + e,
            method: "GET"

        }).then(function (res) {
            console.log(res.data.res);
            $scope.MEReport = res.data.res;
        });
    }

    
}]);



app.directive("fileInput", function () {
    return {

        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var file = elem[0].files[0];
                debugger;
                ngModel.$setViewValue(file);

                //var reader = new FileReader();
                //if (document.getElementById(attrs.filesImageId)) {
                //    reader.onload = function () {
                //        var dataUrl = reader.result;
                //        var output = document.getElementById(attrs.filesImageId);
                //        if (output) {
                //            output.src = dataUrl;
                //        }
                //    };
                //}
                //reader.readAsDataURL(file);
            });
        }
    }
});