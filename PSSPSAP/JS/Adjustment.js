﻿angular.module("SAP").requires.push("ngMaterial");
angular.module("SAP").requires.push('ngSanitize');
angular.module("SAP").requires.push("ui.select");

app.controller('Adjustment', ['$scope', '$http', function ($scope, $http) {
    $scope.alert = 0;
    $scope.alertMsg = '';

    //Drop District
    $http({
        url: "/Common/DropDistrict",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.District = res.data.Ddl;
    });
    //Drop Phase
    $scope.Phase = [{ PKCODE: 1, Name: "Phase 1" }, { PKCODE: 2, Name: "Phase 2" }, { PKCODE: 3, Name: "Phase 3" }];

    //Drop Applicent Types
    $http({
        url: "/Common/DropAppTypes",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.AppTypes = res.data.Ddl;
    });
    //Drop AdjustmrntTypes
    $http({
        url: "/Common/AdjustmrntTypes",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.AdjsTypes = res.data.Ddl;
    });

    //Get All Adjustment 
    $http({
        url: "/Adjustment/GetAllAdjustment",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl)
        $scope.AllAdustmentList = res.data.Ddl;
    });
    $scope.SelectedDis = [];
    $scope.SelectedPhase = [];
    $scope.SelectedType = [];
   
    $scope.AllAdjustment = {
        FKTYPE: "",
        Grop: {},
        EMIS: "",
        NAME: "",
        AMNT: 0,
        IAMNT: 0,
        STEPS: 0,
    }
    $scope.SaveAdjustment = function () {
        $scope.Search = {
            Dist: $scope.SelectedDis,
            Phase: $scope.SelectedPhase,
            appType: $scope.SelectedType
        }
        $scope.AllAdjustment.Grop = $scope.Search;
        console.log($scope.SelectedDis);
        console.log($scope.SelectedPhase);
        console.log($scope.SelectedType);
        console.log($scope.Search);
        console.log($scope.AllAdjustment);
        $http({
            url: "/Adjustment/SaveAdjustment",
            method: "POST",
            data:  $scope.AllAdjustment

        }).then(function (res) {
            if (res.data.Err == "N") {
                alert("Adjustment is apply successfully");
                $scope.DefineRates = null;
                location.reload();
            }
            else {
                alert(res.data.Err);
            }
        })
        }


    $scope.VerifyEmis = function (e) {
        
        $http({
            url: "/Adjustment/VerifyEmis/"+e,
            method: "GET"

        }).then(function (res) {
             
            $scope.SchoolName = res.data.Ddl;
        });
    }



    //Alert Dispose
    $scope.disposAlert = function () {
        $scope.alert = 0;
        $scope.alertMsg = '';
    }

}]);