﻿angular.module("SAP").requires.push("ngMaterial");
app.controller('PaymentCritaria', ['$scope', '$http', function ($scope, $http) {

   
   
    //DropAppTypes
    $http({
        url: "/Common/DropAppTypes",
        method: "GET"

    }).then(function (res) {
        if (res.data.Err == "N") {
            console.log(res.data.Ddl);
            $scope.appTyps = res.data.Ddl;
        }
        else {
            alert(res.data.Err);
        }
    });
    //PCD and MCD 
    $http({
        url: "/Common/DropPCDMCD",
        method: "GET"

    }).then(function (res) {
        if (res.data.Err == "N") {
           
            $scope.PCD = res.data.PCD;
            $scope.MCD = res.data.MCD;
        }
        else {
            alert(res.data.Err);
        }
    });
    //Definng Rates Thair types 
    //Object to submit
    $scope.apptIndex = "";
    $scope.DefineRates = {
        ID : 0,
        FKTYPE: "",
        APPLICANT_TYPE: "",
        RATE: "",
     //  FKPCD: "",
      // FKMCD: "",
        UNIT : ""

    }

    $scope.appindexcat = "0";
    $scope.DefineRatesCat = {
        ID: 0,
        FKTYPE: "",
        APPLICANT_TYPE: "",
        RATE: "",
        UNIT: ""

    }
    $scope.FKPCD = '';
    $scope.FKMCD = '';
    $scope.GetRates = function () {
        $http({
            url: "/PaymentCriteria/GetRates",
            method: "POST",
            data : {pc : $scope.FKPCD , mc : $scope.FKMCD}

        }).then(function (res) {
            if (res.data.Err == "N") {

                $scope.DefinedRates = res.data.res;
            }
            else {
                alert(res.data.Err);
            }
        });
    }
    $scope.SaveRates = function () {

       
        $http({
            url: "/PaymentCriteria/SaveRates",
            method: "POST",
            data: $scope.DefineRates

        }).then(function (res) {
            if (res.data.Err == "N") {
                alert(res.data.res);
                $scope.DefineRates = null;
                location.reload();
            }
            else {
                alert(res.data.Err);
            }
        });
    }
    $scope.EditRate = function (e) {
        $http({
            url: "/PaymentCriteria/EditRate/"+e,
            method: "GET"

        }).then(function (res) {
            if (res.data.Err == "N") {
                console.log(res.data.res);
                $scope.DefineRates = res.data.res;

                $scope.apptIndex = "" + res.data.res.APPLICANT_TYPE;
                $scope.FilterRates($scope.apptIndex);
            }
            else {
                alert(res.data.Err);
            }
        });
    }

    //SaveRates Ctegory Wise/////
    $scope.SaveRatesCat = function () {


        $http({
            url: "/PaymentCriteria/SaveRatesCategoryWise",
            method: "POST",
            data: $scope.DefineRatesCat

        }).then(function (res) {
            if (res.data.Err == "N") {
                alert(res.data.res);
                $scope.DefineRatesCat = null;
                location.reload();
            }
            else {
                alert(res.data.Err);
            }
        });
    }
    //Defining Planties
    $scope.planty = {
        ID: 0,
        INDICATOR: "",
        PENALTY: 0,
        NOOFSCHOOL: 0,
        RMKS : ""
    }
    $scope.SavePlanty = function () {
        $http({
            url: "/PaymentCriteria/SavePlanty",
            method: "POST",
            data: $scope.planty

        }).then(function (res) {
            if (res.data.Err == "N") {
                alert(res.data.res);
                $scope.planty = {
                    ID: 0,
                    INDICATOR: "",
                    PENALTY: 0,
                    NOOFSCHOOL: 0,
                    RMKS: ""
                }
                $scope.Planites = res.data.panties;
            }
            else {
                alert(res.data.Err);
            }
        });
    }

    $scope.EditPlanty = function (index) {
        $scope.planty.ID = $scope.Planites[index].ID;
        $scope.planty.INDICATOR = $scope.Planites[index].INDICATOR;
        $scope.planty.PENALTY = $scope.Planites[index].PENALTY;
        $scope.planty.NOOFSCHOOL = $scope.Planites[index].NOOFSCHOOL;
        $scope.planty.RMKS = $scope.Planites[index].RMKS;

    }

    $scope.SaveRatesType = function () {
        $http({
            url: "/PaymentCriteria/SaveRatesType",
            method: "POST",
            data: { id: $scope.RateType }

        }).then(function (res) {
            if (res.data.Err == "N") {
                alert(res.data.res);
                location.reload();
            }
            else {
                alert(res.data.Err);
            }
        });
    }

    $scope.FilterRates = function (e) {
        $scope.DefineRates.APPLICANT_TYPE ="" + $scope.appTyps[e].PKCODE;
        console.log($scope.appTyps[e].PKCODE)
        console.log($scope.DefineRates.APPLICANT_TYPE)
        //GetRatesType
        $http({
            url: "/Common/GetRatesType/" + $scope.appTyps[e].isC,
            method: "GET",

        }).then(function (res) {
            if (res.data.Err == "N") {
                console.log(res.data.Ddl);
                $scope.rates = res.data.Ddl;
            }
            else {
                alert(res.data.Err);
            }
        });
    }



    ///////category wise filter//////////
    $scope.FilterRatesCat = function (e) {

        if (e !=0)
        {
            $scope.DefineRatesCat.APPLICANT_TYPE = "" + e;
            console.log($scope.appTyps[e].PKCODE)
            console.log($scope.DefineRatesCat.APPLICANT_TYPE)
            //GetRatesType
            $http({
                url: "/Common/GetRatesType/" + $scope.appTyps[e].isC,
                method: "GET",

            }).then(function (res) {
                if (res.data.Err == "N") {
                    console.log(res.data.Ddl);
                    $scope.rates = res.data.Ddl;
                }
                else {
                    alert(res.data.Err);
                }
            });
        }
       
    }
    
}]); 