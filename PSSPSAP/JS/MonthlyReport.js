﻿angular.module("SAP").requires.push("ngMaterial");  
angular.module("SAP").requires.push('ngSanitize');
angular.module("SAP").requires.push("ui.select"); 
angular.module("SAP").requires.push("ui.bootstrap");


 
app.controller('MonthlyUpdate', ['$scope', '$http', function ($scope, $http) {

   


    //Alert
    $scope.alert = 0;
    $scope.alertMsg = '';
    //Alert Dispose
    $scope.disposAlert = function () {
        $scope.alert = 0;
        $scope.alertMsg = '';
    }
    //Drop Phase
    $scope.Phase = [{ PKCODE: 1, Name: "Phase 1" }, { PKCODE: 2, Name: "Phase 2" }, { PKCODE: 3, Name: "Phase 3" }];

    //Drop District
    $http({
        url: "/Common/DropDistrict",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl);
        $scope.District = res.data.Ddl;
    });
    //Drop Applicent Types
    $http({
        url: "/Common/DropAppTypes",
        method: "GET"

    }).then(function (res) {
        console.log(res.data.Ddl);
        $scope.AppTypes = res.data.Ddl;
    });

    //DetailReport Summary Panals 
    $scope.DS = 1;
    $scope.DSR = 0;
    $scope.DetailSummaryPanal = function (e) {
        if (e == 4)
            window.location.href = "/UploadMonthlyReport/Index";
        else
        $scope.DS = e
    }

    $scope.SelectedDis = [];
    $scope.SelectedPhase = [];
    $scope.SelectedType = [];
    $scope.Search = {};
    $scope.Pagination = { Page: 1, PageSize: 10, TotalRecords: 0, Search: "", SortByFieldName: "", SortBy: 'asc', Filters: {} };
 
    //FInd School
    $scope.FindSchool = function () {

      
        $scope.Search = {
            Dist: $scope.SelectedDis,
            Phase: $scope.SelectedPhase,
            appType: $scope.SelectedType
        }
        $scope.Pagination.Filters = $scope.Search;
        console.log($scope.Pagination);

        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/FindSchool",
            method: "POST",
            data: $scope.Pagination

        }).then(function (res) {
            console.log(res.data.res);
            $scope.Schools = res.data.res;
            $scope.Pagination.TotalRecords = res.data.TotalCount
            $scope.DSR = 1;
            $scope.alertMsg = '';
            $scope.alert = 0;
        });
    }

    $scope.pageChanged = function () {
        $scope.FindSchool();
    }
    //Penalties And Adjuestment Part
    $scope.SelectedSchool = [];
    $scope.Selected = function (e, n) {
       
        $scope.s = {
            emis: e,
            name : n
        }
       
        var index = 0;
        for (var i = 0 ; i < $scope.SelectedSchool.length; i++) {
            if ($scope.SelectedSchool[i].emis === $scope.s.emis)
                index = 1;
        }

        if ( index === 1) {
          
            $scope.alertMsg = 'You already Select this school';
            $scope.alert = 1;
        }
        else {
            $scope.SelectedSchool.push($scope.s);

        }


    }

    $scope.removeSchool = function (e) {
        $scope.SelectedSchool.splice(e, 1);
    }

    //Drop Pnalties
    $http({
        url: "/Penalty/GetPnalties",
        method: "GET"

    }).then(function (res) {
        
        $scope.planty = res.data.Ddl;
    });
    $scope.pid = '';

    //Apply ApplyPenalty
    $scope.ApplyPenalty = function () {
        $http({
            url: "/Penalty/ApplyPenaltySelected/",
            method: "POST",
            data: { pid: $scope.pid, model: $scope.SelectedSchool }

        }).then(function (res) {
            if(res.data.Err ==='N')
                alert(res.data.res)
            else
                alert(res.data.Err)
          
        });
    }
 
    ////////////////Summeries/////////////////////Area///////////////////////////

    $scope.DistrictWiseSummery = function () {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/DistrictWiseSummery",
            method: "POST",
           

        }).then(function (res) {
            console.log(res.data.Ddl);
            $scope.DisRep = res.data.Ddl
            $scope.DSR = 2;
            $scope.alertMsg = '';
            $scope.alert = 0;
        });
    }
    
    $scope.CatWiseSummery = function () {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/CatWiseSummery",
            method: "POST",


        }).then(function (res) {
            console.log(res.data.Ddl);
            $scope.CatRep = res.data.Ddl
            $scope.DSR = 3;
            $scope.alertMsg = '';
            $scope.alert = 0;
        });
    }
    $scope.TypeWiseSummery = function () {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/TypeWiseSummery",
            method: "POST",


        }).then(function (res) {
            console.log(res.data.Ddl);
            $scope.TypeRep = res.data.Ddl
            $scope.DSR = 4;
            $scope.alertMsg = '';
            $scope.alert = 0;
        });
    }

    $scope.BG = {
        PKCODE:"0",
        NAME:"",
        PENALTY:"",
        EFACT:"",
        RMKS:"",
        ENABLE_FLAG:"",
        CREATED_BY:"",
        CREATION_DATE:"",
        UPDTED_BY: "",
        UPDATION_DATE: "",
    };

    $scope.CreateGroup = function () {
      
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/CreateGroup",
            method: "POST",
            data: { model: $scope.BG, list: $scope.SelectedSchool }

        }).then(function (res) {
            if(res.data.Err == "N"){
                $scope.BG = null;
                $scope.alertMsg = res.data.res;
                $scope.alert = 1;
            }
            else {
               
                $scope.alertMsg = res.data.Err;
                $scope.alert = 1;
            }
        });
    }

    $scope.ViewBlackGroup = function () {
       
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/ViewGroup",
            method: "GET",
          

        }).then(function (res) {
            console.log(res.data.res);
            if (res.data.Err == "N") {
                console.log(res.data.res);
                $scope.BlackGroups = res.data.res
                $scope.DSR = 5;
                $scope.alertMsg = '';
                $scope.alert = 0;
            }
            else
            {
                $scope.alertMsg = res.data.Err;
                $scope.alert = 1;
            }
        });
    }

    $scope.popup = 0;
    $scope.ShowBlakSchoolDtl = function (e) {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/ShowBlakSchoolDtl/"+e,
            method: "GET",


        }).then(function (res) {
            console.log(res.data.res);
            if (res.data.Err == "N") {
                console.log(res.data.res);
                $scope.bds = res.data.res
                $scope.popup = 1;
                $scope.alertMsg = '';
                $scope.alert = 0;
            }
            else {
                $scope.alertMsg = res.data.Err;
                $scope.alert = 1;
            }
        });
    }

    $scope.DisposPopUp = function () {
        $scope.popup = 0;
    }

    $scope.RemoveFromBlackList = function (e) {
        $scope.alertMsg = 'Please Wait . . .';
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/RemoveFromBlackList/" + e,
            method: "GET",


        }).then(function (res) {
            console.log(res.data.res);
            if (res.data.Err == "N") {
                $scope.bds = res.data.res
                $scope.alertMsg = "Remove Successfully";
                $scope.alert = 1;
            }
            else {
                $scope.alertMsg = res.data.Err;
                $scope.alert = 1;
            }
        });
    }

    ////////////Districtwise not submitted report//////////////


    $scope.GetNotSubmittedSchool = function (e , y) {
        $scope.alertMsg = 'Please Wait . . .';
        if(y == "N")
            $scope.DSR5Titel = "Not Submitted";
        else
            $scope.DSR5Titel = "Submitted";
        $scope.alert = 1;
        $http({
            url: "/MonthlyUpdate/GetNotSubmittedSchool/",
            method: "POST",
            data: {id : e , Type : y}

        }).then(function (res) {
          
            $scope.NotSbM = res.data.res
            $scope.DSR = 6;
            $scope.alertMsg = '';
            $scope.alert = 0;
        });
    }

}]);





