﻿angular.module("SAP").requires.push("ngMaterial");
angular.module("SAP").requires.push('ngSanitize');
angular.module("SAP").requires.push("ui.select");
angular.module("SAP").requires.push("ui.bootstrap");
app.controller('StopRelease', ['$scope', '$http', function ($scope, $http) {
   
    $scope.emis = '';
    $scope.GetEmis = function () {
        
        $http({
            url: '/StopRelease/GetEmis/'+$scope.emis,
            method: 'GET'

        }).then(function (response) {
            console.log(response);
            if (response.data.Err == 'N') {
                $scope.SchoolDetail = response.data.res;
                $scope.NF = "";
            }
            else
            {
                $scope.NF = response.data.Err;
                $scope.SchoolDetail = {};
            }
           

        });
        
    }

    //Get Payments
    $scope.GetPayments = function () {
        $http({
            url: '/StopRelease/GetPayments/' + $scope.emis,
            method: 'GET'

        }).then(function (response) {
            console.log(response);
            if (response.data.Err == 'N') {
                $scope.PAymentDetal = response.data.res;
                
            }
            else {
                alert("No Payments Found");
                console.log(response.data.Err);
            }


        });
    }

    //SaveStopRealese
    $scope.SaveStopRealese = function (index) {
        
        var SRModel = {
            SRNO : $scope.PAymentDetal[index].SRNO,
            SR : $scope.PAymentDetal[index].RELEASE_PAYMENT,
            RMKS: $scope.PAymentDetal[index].RMKS,
            ADD_Pnalty: $scope.PAymentDetal[index].RMADDITIONAL_PENALTYKS,
            ADD_Pnalty_AMT: $scope.PAymentDetal[index].ADDTIONAL_PENALTY_AMNT,
            ADD_Pnalty_RMKS: $scope.PAymentDetal[index].ADDTIONAL_PENALTY_RMKS,


        }
        $http({
            url: '/StopRelease/SaveStopRealese/',
            method: 'POST',
            data: SRModel

        }).then(function (response) {
            console.log(response);
            if (response.data.Err == 'N') {
                alert(response.data.res);

            }
            else {
                alert("Something went wrong!");
                console.log(response.data.Err);
            }


        });
    }

    $scope.CalculateAddPenalty = function (e) {
        $scope.PAymentDetal[e].ADDTIONAL_PENALTY_AMNT = ($scope.PAymentDetal[e].Payment * $scope.PAymentDetal[e].ADDITIONAL_PENALTY) / 100;

    }
}]);





